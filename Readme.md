# GraphViewer 
*GraphViewer* is a JavaScript-based web-application to render three-dimensional graphs. 
It’s written by [Markus Bauer](mailto:s9mkbaue@stud.uni-saarland.de), [Fabian Kosmale](mailto:s9kofabi@stud.uni-saarland.de), [Christian Brossette](mailto:christian.brossette@gmail.com) and [Marc Jose](mailto:s9majose@stud.uni-saarland.de) as a project during the “Mathematik für Informatiker III”-lecture held by Dr. Oliver Labs on the University of Saarland in the winter semester 2012/13. 

To use alle features you have to use a WebGL supporting browser. 

If you want to use the application, follow this link:  

* [https://www.graphviewer.tk/](https://www.graphviewer.tk/)
