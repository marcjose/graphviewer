/**
 * @author Markus
 * 
 * Contains a control that allows to select points in graphs by clicking and so on.
 */



function MouseControl(camera, dom){
    var _this = this;
    
    this.camera = camera;
    this.dom    = dom;
    this.projector = new THREE.Projector();
    
    
    
    /**
     * Returns the clicked graph and face.
     * Result contains: 
     * distance: to cut point
     * point: cutting point,
     * face: face,
     * faceIndex: f,
     * object: graph mesh
     * graph: the graph
     * graphId: the graph's index in graphset
     * vertex: the nearest vertex
     */
    this.getClickedGraph = function(x,y){
        var vector = new THREE.Vector3((x/_this.dom.clientWidth)*2 - 1, 1-(y/_this.dom.clientHeight*2) , 0);
        var raycaster = _this.projector.pickingRay(vector, _this.camera);
        
        var cut = null;
        
        //go through graphs
        for (var i = 0; i<graphset.count(); i++){
            if(graphset.graphs[i].isVisible() && (graphset.graphs[i].mesh !== null)){
                //get intersections
                var inter = raycaster.intersectObject(graphset.graphs[i].mesh, true);
                for (var j = 0; j < inter.length; j++){
                    if (cut===null || cut.distance > inter[j].distance){
                        cut=inter[j];
                        cut.graph = graphset.graphs[i];
                        cut.graphId = i;                        
                    }
                }
            }
        }
        
        if (cut === null) return null;
        
        //get nearest vertex
        var faceIndices = [ 'a', 'b', 'c', 'd' ];
        var n = ( cut.face instanceof THREE.Face3 ) ? 3 : 4;
        var v,p,d, m=Infinity;
        for( var j = 0; j < n; j++ ) {                      
            p = cut.graph.geometry.vertices[ cut.face[ faceIndices[j] ] ];
            d = cut.point.distanceTo(p);
            if (d<m){
                m = d;
                v = p;
            }
        }
        cut.vertex = v;
        
        return cut;
    }
    

    
    /**
     * Called after a single click 
     */
    this.mouseClick = function(event){
        if (engine.controls1.hasRotated()) return; //Rotating
        var pos = _this.getClickedGraph(event.clientX-_this.dom.offsetLeft, event.clientY-_this.dom.offsetTop);
        
        graphset.select(pos);
    }
    
    /**
     * Called after a double click 
     */
    this.mouseDblClick = function(event){
        var pos = _this.getClickedGraph(event.clientX-_this.dom.offsetLeft, event.clientY-_this.dom.offsetTop);
        if (pos !== null){
            pos.graph.dblClick(pos);
        }
    }
    

	this.dom.addEventListener( 'click', this.mouseClick, false );
	this.dom.addEventListener( 'dblclick', this.mouseDblClick, false );

}



