/**
 * @author Markus
 * 
 * Contains an object to represent a graph. It will contain mesh, material, formula and so on. 
 */

var facesBound = 250000; //=500*500, 1000000faces+wireframe


/**
 * Different color functions
 */
var ColorFunctions = {};
//low is green, middle is yellow, high is red. 
ColorFunctions.greenRedHeight = {
    func: function(vec,min,max){
            if(min==max){
                return new THREE.Color(0xffff00);
            }else{
                return new THREE.Color(Math.min(255,Math.round(512*(vec.z-min)/(max-min)))*0x10000
                                    + Math.min(255,Math.round(512*(vec.z-max)/(min-max)))*0x100);
            }
          },
    preview: function(div){
        div.style.backgroundImage = "url(images/colorRYG.png)";
    }
}

ColorFunctions.grayHeight = {
    func: function(vec,min,max){
            if(min==max){
                return new THREE.Color(0x808080);
            }else{
                var x = Math.round(31+192*(vec.z-min)/(max-min));
                var c = new THREE.Color((x*0x10000) + (x*0x100) + x);
                return c;
            }
          },
    preview: function(div){
        div.style.backgroundImage = "url(images/colorGray.png)";
    }
}

ColorFunctions.simpleColor = function(color){
    return {
        func: function(vec,min,max){ return color; },
        preview: function(div){
            div.style.backgroundColor = "#"+color.getHexString();
        }
    };
}
    
ColorFunctions.defaultColor = ColorFunctions.greenRedHeight;

ColorFunctions.list = new Array(ColorFunctions.simpleColor(new THREE.Color(0xff0000)), 
                        ColorFunctions.simpleColor(new THREE.Color(0x008000)), 
                        ColorFunctions.simpleColor(new THREE.Color(0x0000ff)),
                        ColorFunctions.grayHeight, 
                        ColorFunctions.simpleColor(new THREE.Color(0xffff00)), 
                        ColorFunctions.simpleColor(new THREE.Color(0x808080)), 
                        ColorFunctions.simpleColor(new THREE.Color(0xff00ff)), 
                        ColorFunctions.greenRedHeight);





/**
 * A single Graph containing formula, mesh and drawing routines
 */
function Graph(term){
    var _graph = this;
    
    //you can attach a gui element: 
    this.gui = null;
    
    //the function
    this.term = term;
    //this.func;
    this.params = [];
    this.parsedTerm = null;
    this.derivateXTerm = null;
    this.derivateYTerm = null;
    
    this.setTerm = function(term){
        _graph.term = term;
        try{
        	_graph.parsedTerm = Parser.parse(term);
            //_graph.func = parsedTerm.eval;
            _graph.params = [];
            var p = _graph.parsedTerm.listVariables().bag_;
            for (var i=0; i<p.length; i++){
            	if (p[i] !== "x" && p[i] !== "y"){
            		_graph.params.push(p[i]);
            	}
            }
            _graph.derivateXTerm = _graph.parsedTerm.derivate("x");
            _graph.derivateYTerm = _graph.parsedTerm.derivate("y");
            return true;
        }catch(e){
        	return e.message;
        }
    }
    if (term !== undefined){ this.setTerm(term); }
    
    this.hasParameter = function(p){
    	for (var i=0; i<_graph.params.length; i++){
    		if (_graph.params[i] == p) return true;
    	}
    	return false;
    }
    
    this.addParameters = function(a){
    	return a.concat(_graph.params);
    }
    
    //drawing area
    this.drawFromX = -1;
    this.drawFromY = -1;
    this.drawToX   =  1;
    this.drawToY   =  1;
    
    //details
    this.detailsX  = webgl ? 60 : 10;
    this.detailsY  = webgl ? 60 : 10;
    this.wireStep  =  webgl ? 2 : 1;
    
    //coloring
    this.colorFunction = ColorFunctions.defaultColor;
    
    //minimum/maximum of currently rendered part
    this.minValue = NaN;
    this.maxValue = NaN;
    
    //materials
    this.material     = webgl ? new THREE.MeshPhongMaterial({color: 0xffffff, shininess: 50, vertexColors: THREE.VertexColors})
                              : new THREE.MeshPhongMaterial({color: 0x3060ff, shininess: 50});
    this.materialWire = new THREE.MeshBasicMaterial({color: 0x000000, wireframe: true, wireframeLinewidth: 2});
    
    //geometries
    this.geometry = null;
    this.geometryWire = null;
    
    //meshes
    this.mesh = null;
    this.meshWire = null;
    
    
    this.setDrawingArea = function(fx,fy,tx,ty, updateGui){
        if (fx == _graph.drawFromX && fy == _graph.drawFromY && tx == _graph.drawToX && ty == _graph.drawToY) return;
        //adjust details
        //dx*dy=dalt
        //dx/x=dy/y
        // => dx = x*dalt/dx*y
        // => dx = sqrt(dalt*x/y)
        if (fx < tx && fy < ty){
        	if (_graph.detailsXr === undefined || !isFinite(_graph.detailsXr) || Math.abs(_graph.detailsXr-_graph.detailsX)>1) _graph.detailsXr = _graph.detailsX;
        	if (_graph.detailsYr === undefined || !isFinite(_graph.detailsYr) || Math.abs(_graph.detailsYr-_graph.detailsY)>1) _graph.detailsYr = _graph.detailsY;
        	var dAlt = _graph.detailsXr * _graph.detailsYr;
        	_graph.detailsXr = Math.sqrt(dAlt * (tx-fx)/(ty-fy));
        	_graph.detailsYr = Math.sqrt(dAlt * (ty-fy)/(tx-fx));
        	_graph.detailsX = Math.round(_graph.detailsXr);
        	_graph.detailsY = Math.round(_graph.detailsYr);
        }
    	_graph.drawFromX = fx;
    	_graph.drawFromY = fy;
		_graph.drawToX   = tx;
		_graph.drawToY   = ty;
		if (_graph.geometry !== null){
		    _graph.renderGraph();
		}
		if (updateGui !== false){
		    _graph.gui.fromX.value = fx;
		    _graph.gui.fromY.value = fy;
		    _graph.gui.toX.value   = tx;
		    _graph.gui.toY.value   = ty;
		}
    }
    
    
    //visibility handling
    this.visible = true;
    this.visibleWireframe = false;
    this.isVisible = function(){
        return _graph.visible || _graph.visibleWireframe;
    }
    this.setVisible = function(visible, visibleWireframe, buttons){
        //show/hide mesh
        if ((visible != _graph.visible) && _graph.mesh != null){
            if (visible){ engine.scene.add(_graph.mesh)}else{ engine.scene.remove(_graph.mesh)} 
        }
        //show/hide wireframe
        if ((visibleWireframe != _graph.visibleWireframe) && _graph.meshWire != null){
            if (visibleWireframe){ engine.scene.add(_graph.meshWire)}else{ engine.scene.remove(_graph.meshWire)} 
        }
        _graph.visible = visible;
        _graph.visibleWireframe = visibleWireframe;
        if (buttons === false) return;
        if (_graph.gui !== null){
            _graph.gui.btn1.className = visible ? "btn pressed" : "btn";
            _graph.gui.btn2.className = visibleWireframe ? "btn pressed" : "btn";
        }
    }
    
    
    /**
     * renders a function - e.g. returns two geometries: [default, wireframe]
     * Adjusts minValue and maxValue if needed
     * Use wireStep = 0 if you do not need a wireframe geometry
     * Parameters: function, colorfunction (point,min,max -> THREE.color)
     *             area to be drawn: (fromX,fromY) - (toX,toY)
     *             cX,cY: seperate area in cX/cY pieces
     *             wirestep: use every x-th vertices for wireframe
     */
    this.renderFunction = function (f, cf, fromX,fromY, toX,toY, cX, cY, wireStep){
        var geo = new THREE.Geometry();
        if (wireStep > 0){
            var geoWire = new THREE.Geometry();
        }
    
        var holes = [];
        var holesWire = [];
    
        for (var i=0; i <= cX; i++){
            for (var j=0; j <= cY; j++){
                var x = fromX + (i/cX)*(toX-fromX);
                var y = fromY + (j/cY)*(toY-fromY);
                var v = f(x,y);
                if (!isFinite(v)){
                    holes.push(true);
                }else{
                    holes.push(false);
                }
                geo.vertices.push(new THREE.Vector3(x,y,v));  //index: i*(cY+1)+j
                if (wireStep > 0){
                    if (i % wireStep == 0 && j % wireStep == 0){
                        geoWire.vertices.push(new THREE.Vector3(x,y,v));
                        holesWire.push(!isFinite(v));
                    }
                }
            
                if (isFinite(v)){
                    if(!isFinite(_graph.minValue)){
                        _graph.minValue = v;
                        _graph.maxValue = v;
                    }
                    if (v < _graph.minValue){ _graph.minValue = v; }
                    if (v > _graph.maxValue){ _graph.maxValue = v; }
                } 
            }
        }
    
        //generate faces
        for (var i=1; i <= cX; i++){
            for (var j=1; j <= cY; j++){
                if (holes[i*(cY+1)+j]     || holes[i*(cY+1)+j-1]|| 
                    holes[(i-1)*(cY+1)+j] || holes[(i-1)*(cY+1)+j-1]){
                        //not a number => hole!
                }else{              
                    geo.faces.push(new THREE.Face3(i*(cY+1)+j , (i-1)*(cY+1)+j , (i-1)*(cY+1)+j-1));
                    geo.faces.push(new THREE.Face3(i*(cY+1)+j , (i-1)*(cY+1)+j-1 , i*(cY+1)+j-1));
                    geo.faces.push(new THREE.Face3(i*(cY+1)+j , (i-1)*(cY+1)+j-1 , (i-1)*(cY+1)+j));
                    geo.faces.push(new THREE.Face3(i*(cY+1)+j , i*(cY+1)+j-1 , (i-1)*(cY+1)+j-1));
                }
            }
        }
    
        //generate wireframe faces
        if(wireStep > 0){
        for (var i=1; i <= div(cX,wireStep); i++){
            for (var j=1; j <= div(cY, wireStep); j++){
                if( holesWire[i*(div(cY, wireStep)+1)+j] ||   
                    holesWire[i*(div(cY, wireStep)+1)+j-1] ||  
                    holesWire[(i-1)*(div(cY, wireStep)+1)+j-1] ||  
                    holesWire[(i-1)*(div(cY, wireStep)+1)+j]){
                }else{
                    geoWire.faces.push(new THREE.Face4(i*(div(cY, wireStep)+1)+j , 
                                                       i*(div(cY, wireStep)+1)+j-1 , 
                                                   (i-1)*(div(cY, wireStep)+1)+j-1 , 
                                                   (i-1)*(div(cY, wireStep)+1)+j));
                }
            }
        }
        }
    
        _graph.colorGeometry(geo, cf);
    
        geo.computeFaceNormals();
    
        if (wireStep > 0){
            return [geo, geoWire];
        }else{
            return [geo, null];
        }
    }
      
    /**
     * Applies a colorfunction to a geometry 
     */
    this.colorGeometry = function(geo, cf){
        if (!webgl) return;
        var faceIndices = [ 'a', 'b', 'c', 'd' ];
        var face, p, n;
        for ( var i = 0; i < geo.faces.length; i ++ ) {
            face  = geo.faces[ i ];
            n = ( face instanceof THREE.Face3 ) ? 3 : 0;
            //go through vertices
            for( var j = 0; j < n; j++ ) {                      
                p = geo.vertices[ face[ faceIndices[j] ] ];
                face.vertexColors[ j ] = cf(p, _graph.minValue, _graph.maxValue);
            }
        }
    }
      
    /**
     * Sets a new color function and colors the graph
     */
    this.setColorFunction = function(cf){
        if (cf !== undefined) _graph.colorFunction = cf;
        if (!webgl){
            _graph.material.color = _graph.colorFunction.func(new THREE.Vector3(0,0,0), -1,1);
        }
        if (_graph.geometry !== null){
            var v1 = _graph.visible, v2 = _graph.visibleWireframe;
            _graph.setVisible(false,false,false);
            _graph.colorGeometry(_graph.geometry, _graph.colorFunction.func);
            //re-create geometry - needed to draw new
            var geo = new THREE.Geometry();
            geo.vertices = _graph.geometry.vertices;
            geo.faces    = _graph.geometry.faces;
            _graph.geometry = geo;
            _graph.mesh = new THREE.Mesh(_graph.geometry, _graph.material);
            _graph.setVisible(v1,v2); 
        }        
    }
    
    
    
    /**
     * Re-Draws the whole graph in range
     */
    this.renderGraph = function(){
    	if (_graph.gui !== undefined && _graph.gui !== null){
    		_graph.gui.btn4.title = "Decrease resolution for this graph. Current resulution: "+_graph.detailsX+" x "+_graph.detailsY;
    		_graph.gui.btn5.title = "Increase resolution for this graph. Current resulution: "+_graph.detailsX+" x "+_graph.detailsY;
    	}
        var v1 = _graph.visible, v2 = _graph.visibleWireframe;
        _graph.setVisible(false,false,false);
        _graph.minValue = NaN;
        _graph.maxValue = NaN;
        try{
            var a = _graph.renderFunction(_graph.getFunction(), _graph.colorFunction.func, 
                    _graph.drawFromX, _graph.drawFromY, _graph.drawToX, _graph.drawToY, 
                    _graph.detailsX, _graph.detailsY, _graph.wireStep);
        }catch(e){
            _graph.setVisible(v1,v2);
            console.log("Graph: rendering error: "+e.message+"   in "+_graph.term);
            return;
        }
        _graph.geometry = a[0];
        _graph.geometryWire = a[1];
        _graph.mesh = new THREE.Mesh(_graph.geometry, _graph.material);
        _graph.meshWire = new THREE.Mesh(_graph.geometryWire, _graph.materialWire);
        
        _graph.setVisible(v1,v2);       
    }
        
    /**
     * Returns a function. This function takes x,y and returns z (or NaN respectively)
     */
    this.getFunction = function(){
    	var p = graphset.parameters;
        return function(x,y){
            p.x = x;
            p.y = y;
            return _graph.parsedTerm.eval(p);
        }
    }
    
    this.dblClick = function(event){
        //Determine old details
        //x-difference between two points = (toX-fromX)/detailX
        var p1 = _graph.geometry.vertices[ event.face['a'] ];
        var p2 = _graph.geometry.vertices[ event.face['b'] ];
        var p3 = _graph.geometry.vertices[ event.face['c'] ];
        var dx = Math.max(Math.abs(p1.x-p2.x) ,
                 Math.max(Math.abs(p1.x-p3.x) , Math.abs(p2.x-p3.x)));
        var dy = Math.max(Math.abs(p1.y-p2.y) ,
                 Math.max(Math.abs(p1.y-p3.y) , Math.abs(p2.y-p3.y)));
        var detX = Math.round((_graph.drawToX-_graph.drawFromX)/dx);
        var detY = Math.round((_graph.drawToY-_graph.drawFromY)/dy);
        _graph.renderCloser(event.vertex.x, event.vertex.y, detX*2, detY*2); //double accuracy
    }
       
    /**
     * Renders a part of the graph around (x,y) again with more details
     */
    this.renderCloser = function(x,y, newDetX, newDetY){
        var v1 = _graph.visible, v2 = _graph.visibleWireframe;
        _graph.setVisible(false,false,false);
        
        //determine area
        var dx = _graph.drawToX-_graph.drawFromX;
        var dy = _graph.drawToY-_graph.drawFromY;
        var rx = (newDetX>=32*_graph.detailsX?1 : newDetX >= 8*_graph.detailsX?3 : 5)*dx/_graph.detailsX;
        var ry = (newDetY>=32*_graph.detailsY?1 : newDetY >= 8*_graph.detailsY?3 : 5)*dy/_graph.detailsY;
        //get the nearest fx,tx at x+-rx where fx=fromx+i/detailsX*dx
        // x-rx=fromx+i*(tX-fX)/detX <=> i=(x-rx-fromx)*detX/(tX-fX)
        var fromX = _graph.drawFromX + Math.round((x-rx-_graph.drawFromX)*_graph.detailsX/dx)/_graph.detailsX * dx;
        var toX   = _graph.drawFromX + Math.round((x+rx-_graph.drawFromX)*_graph.detailsX/dx)/_graph.detailsX * dx;
        var fromY = _graph.drawFromY + Math.round((y-ry-_graph.drawFromY)*_graph.detailsY/dy)/_graph.detailsY * dy;
        var toY   = _graph.drawFromY + Math.round((y+ry-_graph.drawFromY)*_graph.detailsY/dy)/_graph.detailsY * dy;
        fromX = Math.max(fromX, _graph.drawFromX);
        fromY = Math.max(fromY, _graph.drawFromY);
        toX   = Math.min(toX,   _graph.drawToX);
        toY   = Math.min(toY,   _graph.drawToY);

        //new details, ensure this is ok
        newDetX = Math.round(newDetX * (toX - fromX)/dx);
        newDetY = Math.round(newDetY * (toY - fromY)/dy);
        if (newDetX * newDetY > facesBound){
            if (!gui.askTooManyFaces(newDetX * newDetY)){
                _graph.setVisible(v1,v2);
                return;
            }
        }
                
        //delete all faces from area
        var sf = 0.000000001;
        _graph.geometry = _graph.eraseArea(_graph.geometry, fromX+sf, fromY+sf, toX-sf, toY-sf);
        _graph.geometryWire = _graph.eraseArea(_graph.geometryWire, fromX+sf, fromY+sf, toX-sf, toY-sf);
        
        //calculate new area with new details
        var a = _graph.renderFunction(_graph.getFunction(), _graph.colorFunction.func, 
                    fromX, fromY, toX, toY, 
                    newDetX, newDetY, _graph.wireStep);
        
        //join geometrys
        THREE.GeometryUtils.merge(_graph.geometry, a[0]);
        _graph.geometry.mergeVertices();
        if (_graph.selEvent !== null){ _graph.selEvent.face = _graph.geometry.faces[_graph.geometry.faces.length-2]; } //only needed to determine next calculation's details
        if (a[1] !== null){ THREE.GeometryUtils.merge(_graph.geometryWire, a[1]); }
        _graph.mesh = new THREE.Mesh(_graph.geometry, _graph.material);
        _graph.meshWire = new THREE.Mesh(_graph.geometryWire, _graph.materialWire);
        
        _graph.setVisible(v1,v2);
    }
      
    this.eraseArea = function(geo, fromX, fromY, toX, toY){
        var VinArea = function(v){
            if (v.x < fromX || v.x > toX || v.y < fromY || v.y > toY) return 0;
            if (v.x > fromX && v.x < toX && v.y > fromY && v.y < toY) return 8;
            return 1;
        }
        
        var newgeo = new THREE.Geometry();
        newgeo.vertices = geo.vertices;
        
        var faceIndices = [ 'a', 'b', 'c', 'd' ];
        var face, p, n;
        var newfaces = [];
        for ( var i = geo.faces.length-1; i >= 0 ; i -- ) {
            face  = geo.faces[ i ];
            n = ( face instanceof THREE.Face3 ) ? 3 : 4;
            //go through vertices
            var x = 0;
            for( var j = 0; j < n; j++ ) {                      
                p = geo.vertices[ face[ faceIndices[j] ] ];
                x += VinArea(p);
            }
            if (x < 3){
                newgeo.faces.push(face);
            }
        }
        
        return newgeo;
    }
    
    this.transparent = false;
    this.setTransparent = function(transparent){
        _graph.material.transparent = transparent;
        _graph.transparent = transparent;
        _graph.material.opacity = transparent ? 0.8 : 1;
        if (_graph.gui !== null) _graph.gui.btn3.className = transparent ? "btn pressed" : "btn";
    }
    
    this.remove = function(){
        _graph.setVisible(false,false);
        _graph.deselect(null);
    }
    
    
    // === SELECTION ===
    this.selPoint = null; 
    this.selEvent = null; //the event that selected the point - contains clicked face and so on
    this.selGeometry = new THREE.SphereGeometry(0.02, 16,16);
    this.selMesh     = new THREE.Mesh(this.selGeometry, new THREE.MeshBasicMaterial({color: 0x000000}));
    this.selDX = this.selDY = this.selGradient = null;
    
    /**
     * Used to select a point on this graph 
     */
    this.select = function(event){
        if (_graph.selPoint == event.vertex) return;
        if (_graph.selPoint === null){
            engine.scene.add(_graph.selMesh);
        }
        if (_graph.selDX !== null) _graph.selDX.hide();
        if (_graph.selDY !== null) _graph.selDY.hide();
        if (_graph.selGradient !== null) _graph.selGradient.hide();
        _graph.selMesh.position = event.vertex;
        _graph.selPoint = event.vertex;
        _graph.selEvent = event;
        var p = graphset.parameters;
        p.x = _graph.selPoint.x;
        p.y = _graph.selPoint.y;
        var dx = _graph.derivateXTerm.eval(p);
        var dy = _graph.derivateYTerm.eval(p);        
        gui.setDetails(_graph.selPoint.x, _graph.selPoint.y, _graph.selPoint.z, dx, dy);
        if (isFinite(dx)){
            var v = new THREE.Vector3(1,0,dx);
            v.setLength(dx);
            _graph.selDX = new VectorMesh(v, 0.015, new THREE.MeshBasicMaterial({color: 0xff0000}), true);
            _graph.selDX.mesh.position.copy(_graph.selPoint);
            _graph.selDX.show();
        }
        if (isFinite(dy)){
            v = new THREE.Vector3(0,1,dy);
            v.setLength(dy);
            _graph.selDY = new VectorMesh(v, 0.015, new THREE.MeshBasicMaterial({color: 0x00ff00}), true);
            _graph.selDY.mesh.position.copy(_graph.selPoint);
            _graph.selDY.show();            
        }
        if (isFinite(dx) && isFinite(dy)){
            _graph.selGradient = new VectorMesh(new THREE.Vector3(dx,dy,0), 0.015, new THREE.MeshBasicMaterial({color: 0x0000ff}), true);
            _graph.selGradient.mesh.position.copy(_graph.selPoint);
            _graph.selGradient.show();            
        }        
    }
    
    /**
     * Used to unselect the graph 
     */
    this.deselect = function(event){
        if (_graph.selPoint !== null){
            engine.scene.remove(_graph.selMesh);
            if (_graph.selDX !== null) _graph.selDX.hide();
            if (_graph.selDY !== null) _graph.selDY.hide();
            if (_graph.selGradient !== null) _graph.selGradient.hide();
        }
        _graph.selPoint = null;
    }
    
    /**
     * Returns the estimated surface of the graph. 
     * The surface is guaranteed to be <= the exact surface, up to a little factor ;)
     */
    this.getSurface = function(){
    	//surface of a single (triangle) face
    	var ts = function(face){
    		var v1 = new THREE.Vector3();
    		var v2 = new THREE.Vector3();
    		v1.copy(_graph.geometry.vertices[face.a]); v2.copy(v1);
    		v1.subSelf(_graph.geometry.vertices[face.b]);
    		v2.subSelf(_graph.geometry.vertices[face.c]);
    		v1.crossSelf(v2);
    		return Math.abs(v1.length()/2);
    	}
    	
    	//sum all faces up
    	var s = 0;
    	var i = _graph.geometry.faces.length;
    	while(i--){
    		s += ts(_graph.geometry.faces[i]);
    	}
    	return (s/2); //because I have 4 triangles per rectangle: 2 for the upper and 2 for the lower side. 
    }
    
}




/**
 * Contains a set of Graph(), and some properties both might need
 */
function GraphSet(){
    var _graphset = this;
    
    //all containing graphs
    this.graphs = [];
    this.selected = null;
    
    //drawing area
    this.drawFromX = -1;
    this.drawFromY = -1;
    this.drawToX   =  1;
    this.drawToY   =  1;
      
    //visibility
    this.visible = false;
    this.visibleWireframe = false;
    this.isVisible = function(){
        return _graphset.visible || _graphset.visibleWireframe;
    }
    this.setVisible = function(visible, visibleWireframe){
        _graphset.visible = visible;
        _graphset.visibleWireframe = visibleWireframe;
        for (var i = 0; i < _graphset.graphs.length(); i++){
            _graphset.graphs[i].setVisible(visible, visibleWireframe);
        }
    }
    
    this.parameters = {};
    this.updateParameter = function(param,v){
    	_graphset.parameters[param] = v;
    	var intersect = false;
    	for (var i=0; i<_graphset.graphs.length; i++){
    		if (_graphset.graphs[i].hasParameter(param)){
    		    if (_graphset.graphs[i] === gui.intersect1 || _graphset.graphs[i] === gui.intersect2){
                    intersect = true;
                }
    			if (_graphset.graphs[i].geometry !== null) _graphset.graphs[i].renderGraph();
    		}
    	}
    	
    	if (intersect){
    	    gui.updateIntersection();
    	}
    }
    
    
    /**
     * Passes changed drawFrom/drawTo values to the graphs 
     */
    this.updateDrawingArea = function(){
    	for (var i=0; i<_graphset.graphs.length; i++){
    		_graphset.graphs[i].setDrawingArea(_graphset.drawFromX, _graphset.drawFromY, _graphset.drawToX, _graphset.drawToY);
    	}
    }
    
    
    
    /**
     * adding graphs to list
     * this also changes his drawing area to the default one 
     */
    this.add = function(g, setRenderZone){
        if (setRenderZone !== false){
            g.drawFromX = _graphset.drawFromX;
            g.drawFromY = _graphset.drawFromY;
            g.drawToX   = _graphset.drawToX;
            g.drawToY   = _graphset.drawToY;
        }
        _graphset.graphs.push(g);
    }
    
    //removes a graph from the list
    this.remove = function(g){
        for (var i=0; i<_graphset.graphs.length; i++){
            if (_graphset.graphs[i] === g){
                _graphset.graphs.splice(i,1);
                return true;
            } 
        }       
        return false; 
    }
    
    this.replace = function(x, g) {
    	try {
    		_graphset.graphs[x].setVisible(false, false);
        	_graphset.graphs[x] = g;
        	g.renderGraph();	
    	} catch (e) {_graphset.add(g);}
    }
    
    this.count = function(){
        return _graphset.graphs.length;
    }   
    
    this.select = function(event){
        if (event === null){
            if (_graphset.selected === null) return;
            _graphset.selected.deselect(event);
            _graphset.selected = null;
            gui.setDetails(null);
        }else{
            if (_graphset.selected !== event.graph){
                if (_graphset.selected !== null){
                    _graphset.selected.deselect(event);
                }
                _graphset.selected = event.graph;
            }
            _graphset.selected.select(event);
        }
    }
    
}
