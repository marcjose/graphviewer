/**
 * @author Markus
 * 
 * Marching sqares algorithm, 2d version implemented after http://en.wikipedia.org/wiki/Marching_squares
 * 
 */



//Creators for function table
function makeSingleTriangle(b1,b2, e){
    return function(geo,c){
        var v11 = c.getBorderV(b1,0,true), v12 = c.getBorderV(b1,1,true);
        var v21 = c.getBorderV(b2,0,true), v22 = c.getBorderV(b2,1,true);
        var v31 = c.getEdgeV(e),      v32 = c.getEdgeV(e+1);
        geo.faces.push(new THREE.Face3(v11, v21, v31));
        geo.faces.push(new THREE.Face3(v22, v12, v32));
        geo.faces.push(new THREE.Face4(v11, v12, v22, v21));
    }
}

function makeSingleTrapezoid(b1,b2){
    return function(geo,c){
        var v111 = c.getBorderV(b1,0,false), v112 = c.getBorderV(b1,1,false);
        var v121 = c.getBorderV(b1,2,false), v122 = c.getBorderV(b1,3,false);
        var v211 = c.getBorderV(b2,0,false), v212 = c.getBorderV(b2,1,false);
        var v221 = c.getBorderV(b2,2,false), v222 = c.getBorderV(b2,3,false);
        geo.faces.push(new THREE.Face4(v111,v221,v211,v121));
        geo.faces.push(new THREE.Face4(v122,v212,v222,v112));
        geo.faces.push(new THREE.Face4(v111,v112,v222,v221));
        geo.faces.push(new THREE.Face4(v121,v211,v212,v122));
    }
}

function makeSingleRect1(e1,b1,b2,e2){
    return function(geo,c){
        var ve11 = c.getEdgeV(e1), ve12 = c.getEdgeV(e1+1);
        var ve21 = c.getEdgeV(e2), ve22 = c.getEdgeV(e2+1); 
        var vb11 = c.getBorderV(b1,0,true), vb12=c.getBorderV(b1,1,true);
        var vb21 = c.getBorderV(b2,0,true), vb22=c.getBorderV(b2,1,true);
        geo.faces.push(new THREE.Face4(ve11,vb11,vb21,ve21));
        geo.faces.push(new THREE.Face4(ve22,vb22,vb12,ve12));
        geo.faces.push(new THREE.Face4(vb21,vb11,vb12,vb22));
    }
}

function makeSingleHexagon(b, e){
    return function(geo,c){
        var vb11 = c.getBorderV(b,0,false), vb12 = c.getBorderV(b,1,false);
        var vb21 = c.getBorderV(b,2,false), vb22 = c.getBorderV(b,3,false);
        var vb31 = c.getBorderV((b+1)%4,0,true), vb32 = c.getBorderV((b+1)%4,1,true);
        var vb41 = c.getBorderV((b+3)%4,0,true), vb42 = c.getBorderV((b+3)%4,1,true);
        var ve11 = c.getEdgeV(e),       ve12 = c.getEdgeV(e+1);
        var ve21 = c.getEdgeV((e+2)%8), ve22 = c.getEdgeV((e+3)%8);
        //top
        geo.faces.push(new THREE.Face3(vb11,vb41,ve21));
        geo.faces.push(new THREE.Face3(ve11,vb31,vb21));
        geo.faces.push(new THREE.Face4(vb21,vb11,ve21,ve11));
        //bottom
        geo.faces.push(new THREE.Face3(ve22,vb42,vb12));
        geo.faces.push(new THREE.Face3(vb22,vb32,ve12));
        geo.faces.push(new THREE.Face4(ve12,ve22,vb12,vb22));
        //sides
        geo.faces.push(new THREE.Face4(vb11,vb12,vb42,vb41));
        geo.faces.push(new THREE.Face4(vb21,vb31,vb32,vb22));
    }
}

function makeSingleHexagon2(b,e){
    return function(geo,c){
        var vb11 = c.getBorderV(b,0,true),        vb12 = c.getBorderV(  b    ,1,true);
        var vb21 = c.getBorderV((b+1)%4,0,true),  vb22 = c.getBorderV((b+1)%4,1,true);
        var vb31 = c.getBorderV((b+2)%4,0,true),  vb32 = c.getBorderV((b+2)%4,1,true);
        var vb41 = c.getBorderV((b+3)%4,0,true),  vb42 = c.getBorderV((b+3)%4,1,true);
        var ve11 = c.getEdgeV(e),       ve12 = c.getEdgeV(e+1);
        var ve21 = c.getEdgeV((e+4)%8), ve22 = c.getEdgeV((e+5)%8);
        //top
        geo.faces.push(new THREE.Face3(vb11,vb21,ve11));
        geo.faces.push(new THREE.Face3(vb31,vb41,ve21));
        geo.faces.push(new THREE.Face4(vb41,vb31,vb21,vb11));
        //bottom
        geo.faces.push(new THREE.Face3(ve12,vb22,vb12));
        geo.faces.push(new THREE.Face3(ve22,vb42,vb32));
        geo.faces.push(new THREE.Face4(vb12,vb22,vb32,vb42));
        //sides
        geo.faces.push(new THREE.Face4(vb41,vb11,vb12,vb42));
        geo.faces.push(new THREE.Face4(vb21,vb31,vb32,vb22));
    }
}

function singleSquare(geo,c){ //1111
    var e = new Array(8);
    for (var i = 0; i < 8; i++){
        e[i] = c.getEdgeV(i);
    }
    geo.faces.push(new THREE.Face4(e[6],e[4],e[2],e[0]));
    geo.faces.push(new THREE.Face4(e[1],e[3],e[5],e[7]));
}

function makeSinglePentagon1(b,e){
    return function(geo,c){
        var vb11 = c.getBorderV(  b    ,0,true),  vb12 = c.getBorderV(  b    ,1,true);
        var vb21 = c.getBorderV((b+1)%4,0,true),  vb22 = c.getBorderV((b+1)%4,1,true);
        var ve11 = c.getEdgeV(e),       ve12 = c.getEdgeV(e+1);
        var ve21 = c.getEdgeV((e+2)%8), ve22 = c.getEdgeV((e+3)%8);
        var ve31 = c.getEdgeV((e+4)%8), ve32 = c.getEdgeV((e+5)%8);
        //top
        geo.faces.push(new THREE.Face4(vb11,ve31,ve11,vb21));
        geo.faces.push(new THREE.Face3(ve11,ve31,ve21));
        //bottom
        geo.faces.push(new THREE.Face4(vb22,ve12,ve32,vb12));
        geo.faces.push(new THREE.Face3(ve22,ve32,ve12));
        //side
        geo.faces.push(new THREE.Face4(vb11,vb21,vb22,vb12));
    }
}

function makeSinglePentagon2(b1,b2,e,b3){
    return function(geo,c){
        var vb11 = c.getBorderV(b1,0,false), vb12 = c.getBorderV(b1,1,false);
        var vb21 = c.getBorderV(b1,2,false), vb22 = c.getBorderV(b1,3,false);
        var vb31 = c.getBorderV(b2,0,true),  vb32 = c.getBorderV(b2,1,true);
        var vb41 = c.getBorderV(b3,0,true),  vb42 = c.getBorderV(b3,1,true);
        var ve1  = c.getEdgeV(e),            ve2  = c.getEdgeV(e+1);
        //top
        geo.faces.push(new THREE.Face3(vb21,vb11,vb31));
        geo.faces.push(new THREE.Face3(vb11,vb41,vb31));
        geo.faces.push(new THREE.Face3(vb41,ve1 ,vb31));
        //bottom
        geo.faces.push(new THREE.Face3(vb32,vb12,vb22));
        geo.faces.push(new THREE.Face3(vb32,vb42,vb12));
        geo.faces.push(new THREE.Face3(vb32,ve2 ,vb42));
        //side
        geo.faces.push(new THREE.Face4(vb21,vb31,vb32,vb22));
        geo.faces.push(new THREE.Face4(vb41,vb11,vb12,vb42));
    }
}

function sided8(geo,c){
        var m = c.getMiddlePoint();
        if (m == c.points[0]){
            makeSingleTrapezoid(3,2)(geo,c);
            makeSingleTrapezoid(0,1)(geo,c);
        }else if (m == 1){
            //get all the vertices
            for (var i=0; i<4; i++) for (var j=0; j<4; j++) c.getBorderV(i,j,false);
            geo.faces.push(new THREE.Face4(c.borderVertices[0][0], c.borderVertices[3][0], c.borderVertices[2][0], c.borderVertices[1][0]));
            geo.faces.push(new THREE.Face4(c.borderVertices[3][1], c.borderVertices[2][1], c.borderVertices[1][1], c.borderVertices[0][1]));
            for (var i=1; i<5; i++){
                geo.faces.push(new THREE.Face3(c.borderVertices[i%4][0], c.borderVertices[i-1][2], c.borderVertices[i-1][0]));
                geo.faces.push(new THREE.Face3(c.borderVertices[i-1][1], c.borderVertices[i-1][3], c.borderVertices[i%4][1]));
                geo.faces.push(new THREE.Face4(c.borderVertices[i-1][2], c.borderVertices[i%4][0], c.borderVertices[i%4][1], c.borderVertices[i-1][3])); //sides
            }
        }else{
            makeSingleTrapezoid(1,2)(geo,c);
            makeSingleTrapezoid(0,3)(geo,c);
        }
}

function makeSided6(b,e){
    return function(geo,c){
        if (c.getMiddlePoint() == 1){
            makeSingleHexagon2(b,e)(geo,c);
        }else{
            makeSingleTriangle(b,(b+1)%4,e)(geo,c);
            makeSingleTriangle((b+2)%4,(b+3)%4,(e+4)%8)(geo,c);
        }        
    }
}

function makeSided7(b,e){
    return function(geo,c){ 
        if (c.getMiddlePoint() == 1){
            var vb11  = c.getBorderV(  b    ,0,true),   vb12  = c.getBorderV(  b    ,1,true);
            var vb21  = c.getBorderV((b+1)%4,0,true),   vb22  = c.getBorderV((b+1)%4,1,true);
            var vb311 = c.getBorderV((b+2)%4,0,false),  vb312 = c.getBorderV((b+2)%4,1,false);
            var vb321 = c.getBorderV((b+2)%4,2,false),  vb322 = c.getBorderV((b+2)%4,3,false);
            var vb411 = c.getBorderV((b+3)%4,0,false),  vb412 = c.getBorderV((b+3)%4,1,false);
            var vb421 = c.getBorderV((b+3)%4,2,false),  vb422 = c.getBorderV((b+3)%4,3,false);
            var ve1   = c.getEdgeV(e),                  ve2   = c.getEdgeV(e+1);
            //top
            geo.faces.push(new THREE.Face3(vb11,vb21,ve1));
            geo.faces.push(new THREE.Face3(vb421,vb411,vb11));
            geo.faces.push(new THREE.Face3(vb321,vb311,vb21));
            geo.faces.push(new THREE.Face4(vb11,vb411,vb321,vb21));
            //bottom
            geo.faces.push(new THREE.Face3(ve2,vb22,vb12));
            geo.faces.push(new THREE.Face3(vb12,vb412,vb422));
            geo.faces.push(new THREE.Face3(vb22,vb312,vb322));
            geo.faces.push(new THREE.Face4(vb22,vb322,vb412,vb12));
            //sides
            geo.faces.push(new THREE.Face4(vb21,vb311,vb312,vb22));
            geo.faces.push(new THREE.Face4(vb321,vb411,vb412,vb322));
            geo.faces.push(new THREE.Face4(vb421,vb11,vb12,vb422));
            
        }else{
            makeSingleTriangle(b,(b+1)%4,e)(geo,c);
            makeSingleTrapezoid((b+3)%4, (b+2)%4)(geo,c);
        }
    }
}






//FUNCTION TABLE
//usage: (geo,c)
var ftable = Array(81);

//=== no contour ===
ftable[0] = ftable[80] = function(geo,c){  //0000+2222
}

//=== single triangle ===
ftable[79] = ftable[1]  = makeSingleTriangle(2,3, 6); //2221 //0001
ftable[77] = ftable[3]  = makeSingleTriangle(1,2, 4); //2212 //0010
ftable[71] = ftable[9]  = makeSingleTriangle(0,1, 2); //2122 //0100
ftable[53] = ftable[27] = makeSingleTriangle(3,0, 0); //1222 //1000

//===single trapezoid===
ftable[78] = ftable[2]  = makeSingleTrapezoid(3,2); //2220 //0002
ftable[74] = ftable[6]  = makeSingleTrapezoid(1,2);  //2202 //0020
ftable[62] = ftable[18] = makeSingleTrapezoid(0,1); //2022 //0200
ftable[26] = ftable[54] = makeSingleTrapezoid(0,3);  //0222 //2000

//===single rectangle===
ftable[4]  = ftable[76] = makeSingleRect1(4,1,3,6); //0011 //2211 
ftable[12] = ftable[68] = makeSingleRect1(2,0,2,4); //0110 //2112
ftable[36] = ftable[44] = makeSingleRect1(0,3,1,2); //1100 //1122
ftable[28] = ftable[52] = makeSingleRect1(6,2,0,0); //1001 //1221
ftable[72] = ftable[8]  = makeSingleTrapezoid(1,3); //2200 //0022
ftable[56] = ftable[24] = makeSingleTrapezoid(0,2); //2002 //0220

//===single hexagon===
ftable[22] = ftable[58] = makeSingleHexagon(0,4); //0211 //2011
ftable[66] = ftable[14] = makeSingleHexagon(3,2); //2110 //0112
ftable[38] = ftable[42] = makeSingleHexagon(2,0); //1102 //1120
ftable[34] = ftable[46] = makeSingleHexagon(1,6); //1021 //1201
ftable[64] = ftable[16] = makeSingleHexagon2(0,2); //2101 //0121
ftable[32] = ftable[48] = makeSingleHexagon2(3,0); //1012 //1210

//===single square===
ftable[40] = singleSquare; //1111

//==single pentagon===
ftable[49] = ftable[31] = makeSinglePentagon1(0,4); //1211 //1011
ftable[67] = ftable[13] = makeSinglePentagon1(3,2); //2111 //0111
ftable[41] = ftable[39] = makeSinglePentagon1(2,0); //1112 //1110
ftable[43] = ftable[37] = makeSinglePentagon1(1,6); //1121 //1101

ftable[45] = ftable[35] = makeSinglePentagon2(1,3,0,0); //1200 //1022
ftable[15] = ftable[65] = makeSinglePentagon2(2,0,2,1); //0120 //2102
ftable[5]  = ftable[75] = makeSinglePentagon2(3,1,4,2); //0012 //2210
ftable[55] = ftable[25] = makeSinglePentagon2(0,2,6,3); //2001 //0221
ftable[29] = ftable[51] = makeSinglePentagon2(2,3,0,0); //1002 //1220
ftable[63] = ftable[17] = makeSinglePentagon2(3,0,2,1); //2100 //0122
ftable[21] = ftable[59] = makeSinglePentagon2(0,1,4,2); //0210 //2012
ftable[7]  = ftable[73] = makeSinglePentagon2(1,2,6,3); //0021 //2201

//===8-sided===
ftable[60] = ftable[20] = sided8; //2020 //0202

//===6-sided===
ftable[10] = ftable[70] = makeSided6(0,2); //0101 //2121
ftable[30] = ftable[50] = makeSided6(3,0); //1010 //1212

//===7-sided===
ftable[69] = ftable[11] = makeSided7(0,2); //2120 //0102
ftable[61] = ftable[19] = makeSided7(2,6); //2021 //0201
ftable[47] = ftable[33] = makeSided7(3,0); //1202 //1020
ftable[23] = ftable[57] = makeSided7(1,4); //0212 //2010













function Cell(x,y,x2,y2,geo,height,f1,f2){
    this.values = new Array(4);  //height
    this.points = new Array(4);  //012
    this.borderVertices = new Array(new Array(-1,-1,-1,-1),new Array(-1,-1,-1,-1),new Array(-1,-1,-1,-1),new Array(-1,-1,-1,-1));  //top right bottom left, 2-4 for each. Inner order according to outer
    this.edgeVertices   = new Array(-1,-1,-1,-1,-1,-1,-1,-1);  //topleft topright bottomright bottomleft, 2 for each
    this.delta = 0;
    
    this.getBorderV = function(b,i,middle){
        if (this.borderVertices[b][i] >= 0) return this.borderVertices[b][i];
        var fac = middle ? 1/2 : i<2 ? 1/3 : 2/3;
        var px,py,pz;
        if (b==0){
            px = x+fac*(x2-x); py=y2;    pz = (1-fac)*this.values[0]+fac*this.values[1];
        }else if(b==1){
            px = x2; py = y2-fac*(y2-y); pz = (1-fac)*this.values[1]+fac*this.values[2];
        }else if(b==2){
            px = x2-fac*(x2-x); py=y;    pz = (1-fac)*this.values[2]+fac*this.values[3];
        }else{
            px = x; py = y+fac*(y2-y);   pz = (1-fac)*this.values[3]+fac*this.values[0];
        }
        pz = i%2==0? pz+height : pz-height; 
        geo.vertices.push(new THREE.Vector3(px,py,pz));
        this.borderVertices[b][i] = geo.vertices.length-1;
        return this.borderVertices[b][i];
    }
    
    this.getEdgeV = function(e){
        if (this.edgeVertices[e] >= 0) return this.edgeVertices[e];
        var px = e<2||e>5 ? x : x2;
        var py = e<4 ? y2 : y;
        var pz = this.values[e>>1];
        pz = (e%2)==0? pz+height : pz-height; 
        geo.vertices.push(new THREE.Vector3(px,py,pz));
        this.edgeVertices[e] = geo.vertices.length-1;
        return this.edgeVertices[e];
    }
    
    
    this.setValue = function(v1,v2,i,delta){
        if (isFinite(v1)){
            if (isFinite(v2)){
                this.values[i] = (v1+v2)/2;
            }else{
                this.values[i] = v1;
            }
        }else{
            if (isFinite(v2)){
                this.values[i] = v2;
            }else{
                this.values[i] = 0;
            }
        }
        if (v1 !== NaN && v2 !== NaN){
            this.points[i] = v1<v2-delta ? 0 : v1>v2+delta? 2 : 1
        }else if(this.points[0] !== undefined && this.points[2] !== undefined){
            this.points[i] = Math.round((this.points[0]+this.points[2])/2);
        }else{
            this.points[i] = 0;
        }
        this.delta = delta;
    }
    
    this.getFromLeft = function(c){
        this.values[0] = c.values[1];
        this.values[3] = c.values[2];
        this.points[0] = c.points[1];
        this.points[3] = c.points[2];
        this.borderVertices[3] = new Array(c.borderVertices[1][2],c.borderVertices[1][3],c.borderVertices[1][0],c.borderVertices[1][1]);
        this.edgeVertices[0] = c.edgeVertices[2];
        this.edgeVertices[1] = c.edgeVertices[3];
        this.edgeVertices[6] = c.edgeVertices[4];
        this.edgeVertices[7] = c.edgeVertices[5];
    }
    
    this.getFromBottom = function(c){
        this.values[2] = c.values[1];
        this.values[3] = c.values[0];
        this.points[2] = c.points[1];
        this.points[3] = c.points[0];
        this.borderVertices[2] = new Array(c.borderVertices[0][2],c.borderVertices[0][3],c.borderVertices[0][0],c.borderVertices[0][1]);
        this.edgeVertices[4] = c.edgeVertices[2];
        this.edgeVertices[5] = c.edgeVertices[3];
        this.edgeVertices[6] = c.edgeVertices[0];
        this.edgeVertices[7] = c.edgeVertices[1];
    }
    
    this.getCode = function(){
        return this.points[0]*27 + this.points[1]*9 + this.points[2]*3 + this.points[3];
    }
    
    this.getMiddlePoint = function(){
        var v1 = f1((x+x2)/2 , (y+y2)/2);
        var v2 = f2((x+x2)/2 , (y+y2)/2);
        if (v1 !== NaN && v2 != NaN){
            this.points[i] = v1<v2-this.delta ? 0 : v1>v2+this.delta? 2 : 1
        }else{
            this.points[i] = 0;
        }
    }
}


function closeCell(geo,c,b){
    var e1 = 2*b, e2 = (2*b+2) % 8;
    if (c.borderVertices[b][0] >= 0 && c.borderVertices[b][2] >= 0){
        geo.faces.push(new THREE.Face4(c.borderVertices[b][0],c.borderVertices[b][2],c.borderVertices[b][3],c.borderVertices[b][1]));
    }else if(c.borderVertices[b][0] >= 0){
        if(c.edgeVertices[e1] >= 0){
            geo.faces.push(new THREE.Face4(c.edgeVertices[e1], c.borderVertices[b][0], c.borderVertices[b][1], c.edgeVertices[e1+1]));
        }
        if(c.edgeVertices[e2] >= 0){
            geo.faces.push(new THREE.Face4(c.borderVertices[b][0], c.edgeVertices[e2], c.edgeVertices[e2+1], c.borderVertices[b][1]));
        }
    }else if(c.edgeVertices[e1] >= 0 && c.edgeVertices[e2] >= 0){
        geo.faces.push(new THREE.Face4(c.edgeVertices[e1], c.edgeVertices[e2], c.edgeVertices[e2+1], c.edgeVertices[e1+1]));
    }
}



function calculateCuttingGeometry(g1,g2){
	//console.log('Creating mesh between '+g1.term+' and '+g2.term);
	
    //parameters
    var detailsX = Math.max(g1.detailsX, g2.detailsX);
    var detailsY = Math.max(g1.detailsY, g2.detailsY);
    
    var geo = new THREE.Geometry();
    var f1 = g1.getFunction();
    var f2 = g2.getFunction();
    var fromX = Math.max(g1.drawFromX, g2.drawFromX);
    var toX   = Math.min(g1.drawToX, g2.drawToX);
    var dx    = toX-fromX;
    var fromY = Math.max(g1.drawFromY, g2.drawFromY);
    var toY   = Math.min(g1.drawToY, g2.drawToY);
    var dy    = toY-fromY;
    var height= (dx+dy)/(3*(detailsX+detailsY));
    
    
    //lets build a cell grid
    var cells = new Array(detailsX);
    var c;
    for (var i=0; i<cells.length; i++){ cells[i] = new Array(detailsY); }
    
    
    //now we fill the cells. We try to use everything we know
    for (var i=0; i<detailsX; i++){ //x
        for (var j=0; j<detailsY; j++){ //y
            var x  = fromX + (i/detailsX)*dx,      y  = fromY + (j/detailsY)*dy;
            var x2 = fromX + ((i+1)/detailsX)*dx,  y2 = fromY + ((j+1)/detailsY)*dy;
            cells[i][j] = new Cell(x, y, x2, y2, geo, height, f1,f2);
            c = cells[i][j];
            if (j==0){         c.setValue(f1(x2,y), f2(x2,y), 2, height)}else{c.getFromBottom(cells[i][j-1])}  //bottom right
            if (i==0){         c.setValue(f1(x,y2), f2(x,y2), 0, height)}else{c.getFromLeft(cells[i-1][j])}  //top left
            if (i==0 && j==0){ c.setValue(f1(x,y),  f2(x,y) , 3, height)}    //bottom left
            c.setValue(f1(x2,y2), f2(x2,y2), 1, height);                     //top right
            
            //calculate!
            ftable[c.getCode()](geo,c);
        }
    }
    
    //build a value grid for each edgepoint
    //we need later: 0/1/2, 1=inner   and assumed height
    
    //then we build the cells and apply the table, creating/linking vertices
    //each cell needs: edge vertices at tblr, 2 or 4, and maybe vertices for the 4 corners (2 for each)
    //make sure you link vertices if they exist
    //build faces instead?^
    
    
    //close border cells
    for (var i=0; i<detailsX; i++){
        closeCell(geo,cells[i][0], 2);
        closeCell(geo,cells[i][detailsY-1], 0);
    }
    for (var i=0; i<detailsY; i++){
        closeCell(geo,cells[0][i], 3);
        closeCell(geo,cells[detailsX-1][i], 1);
    }
    
    return geo;
}

