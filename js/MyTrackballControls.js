/**
 * @author Eberhard Graether / http://egraether.com/
 * Modified by Markus
 */

THREE.TrackballControls = function ( object, domElement ) {

	THREE.EventDispatcher.call( this );

	var _this = this;
	var STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2 };

	this.object = object;
	this.domElement = ( domElement !== undefined ) ? domElement : document;

	// API

	this.enabled = true;

	this.screen = { width: 0, height: 0, offsetLeft: 0, offsetTop: 0 };
	this.radius = ( this.screen.width + this.screen.height ) / 4;

	this.rotateSpeed = 1.0;
	this.rotateXSpeedup = 1.5;
	this.maxXYInclination = 1;
	this.zoomSpeed = 1.2;
	this.panSpeed = 0.3;

	this.noRotate = false;
	this.noZoom = false;
	this.noPan = false;

	this.staticMoving = false;
	this.dynamicDampingFactor = 0.2;

	this.minDistance = 0;
	this.maxDistance = Infinity;

	this.keys = [ 65 /*A*/, 83 /*S*/, 68 /*D*/ ];
	
	this.onzoom = undefined;
	this.callOnZoom = false;
	this.sliderZoomFactor = -1;

	// internals

	this.target = new THREE.Vector3();

	var lastPosition = new THREE.Vector3();

	var _state = STATE.NONE,
	_prevState = STATE.NONE,
	_hasrotated = false, 

	_eye = new THREE.Vector3(),  //my actual camera looking-from direction

	_rotateStart = new THREE.Vector3(),
	_rotateEnd = new THREE.Vector3(),

	_zoomStart = new THREE.Vector2(),
	_zoomEnd = new THREE.Vector2(),

	_panStart = new THREE.Vector2(),
	_panEnd = new THREE.Vector2();

    //exclude: 
    this._eye = _eye;
    
	// events

	var changeEvent = { type: 'change' };


	// methods

	this.handleResize = function () {

		this.screen.width = window.innerWidth;
		this.screen.height = window.innerHeight;

		this.screen.offsetLeft = 0;
		this.screen.offsetTop = 0;

		this.radius = ( this.screen.width + this.screen.height ) / 4;
	};

	this.handleEvent = function ( event ) {

		if ( typeof this[ event.type ] == 'function' ) {

			this[ event.type ]( event );

		}

	};

	this.getMouseOnScreen = function ( clientX, clientY ) {

		return new THREE.Vector2(
			( clientX - _this.screen.offsetLeft ) / _this.radius * 0.5,
			( clientY - _this.screen.offsetTop ) / _this.radius * 0.5
		);

	};

	this.getMouseProjectionOnBall = function ( clientX, clientY ) {
		var aX = Math.asin((clientX - _this.screen.width * 0.5 - _this.screen.offsetLeft)*2/_this.screen.width);
		var aY = Math.asin((_this.screen.height * 0.5 + _this.screen.offsetTop - clientY)*2/_this.screen.height);
		return new THREE.Vector3(aX, aY, 0);
	};
	
	this.isRotating = function(){
	    return Math.round(50*(_rotateEnd.x-_rotateStart.x)) != 0 || Math.round(50*(_rotateEnd.y-_rotateStart.y)) != 0;
	}
	
	this.hasRotated = function(){
	    if(_this._hasrotated){
	       _this._hasrotated = false;
	       return true;
	    }else{
	        return _this.isRotating();
	    }
	    
	}


	this.rotateCamera = function () {	    
	    var daX = _rotateEnd.x - _rotateStart.x;
	    var daY = _rotateEnd.y - _rotateStart.y;
	    
	    var yAxis = _eye.clone();
	        yAxis.normalize();
	    	yAxis.crossSelf(_this.object.up);
	    	yAxis.normalize();
	    	
	    if (! _this.staticmove){
	    	daX *= _this.dynamicDampingFactor;
	    	daY *= _this.dynamicDampingFactor;
	    }
	    
	    var eyeold = _eye.clone();
	    
	    //rotate
	    if (daX != 0 || daY != 0){
	    	quaternion = new THREE.Quaternion();
	    	quaternion.setFromAxisAngle(new THREE.Vector3(0,0,1), -daX*_this.rotateXSpeedup*_this.rotateSpeed);
	    	quaternion.multiplyVector3(_eye);
	    	quaternion.setFromAxisAngle(yAxis, -daY*_this.rotateSpeed);
	    	quaternion.multiplyVector3(_eye);
	    	
	    	_rotateStart.x = _rotateStart.x + daX;
	    	_rotateStart.y = _rotateStart.y + daY;
	    	
	    	engine.camera.up.set(0,0,1);
	    }
	    
	    //up/down lock
	    if (_eye.x != 0 || _eye.y != 0){
	    	if (Math.pow(_eye.z,2) / (Math.pow(_eye.x,2)+Math.pow(_eye.y,2)) > _this.maxXYInclination){
	    	
                var l = _eye.length();
                if (_eye.z > 0){
                    _eye.z = Math.sqrt((Math.pow(_eye.x,2)+Math.pow(_eye.y,2))*_this.maxXYInclination);
                }else{
                    _eye.z = -Math.sqrt((Math.pow(_eye.x,2)+Math.pow(_eye.y,2))*_this.maxXYInclination);
                }
                _eye.normalize();
                _eye.multiplyScalar(l);
            }
        }else{
        	_eye.set(eyeold);
        }
	};

	this.zoomCamera = function () {

		var factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

		if ( factor !== 1.0 && factor > 0.0 ) {

			_eye.multiplyScalar( factor );

			if ( _this.staticMoving ) {

				_zoomStart.copy( _zoomEnd );

			} else {

				_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

			}
			
			if (_this.onzoom !== undefined){
				_this.onzoom(_eye.length());
			}

		}
		if (_this.callOnZoom){
			_this.onzoom(_eye.length());
			_this.callOnZoom = false;
		}

	};

	this.panCamera = function () {

		var mouseChange = _panEnd.clone().subSelf( _panStart );

		if ( mouseChange.lengthSq() ) {

			mouseChange.multiplyScalar( _eye.length() * _this.panSpeed );

			var pan = _eye.clone().crossSelf( _this.object.up ).setLength( mouseChange.x );
			pan.addSelf( _this.object.up.clone().setLength( mouseChange.y ) );

			_this.object.position.addSelf( pan );
			_this.target.addSelf( pan );

			if ( _this.staticMoving ) {

				_panStart = _panEnd;

			} else {

				_panStart.addSelf( mouseChange.sub( _panEnd, _panStart ).multiplyScalar( _this.dynamicDampingFactor ) );

			}

		}

	};

	this.checkDistances = function () {

		if ( !_this.noZoom || !_this.noPan ) {

			if ( _this.object.position.lengthSq() > _this.maxDistance * _this.maxDistance ) {

				_this.object.position.setLength( _this.maxDistance );

			}

			if ( _eye.lengthSq() < _this.minDistance * _this.minDistance ) {

				_this.object.position.add( _this.target, _eye.setLength( _this.minDistance ) );

			}

		}

	};

	this.update = function () {

		_eye.copy( _this.object.position ).subSelf( _this.target );

		if ( !_this.noRotate ) {

			_this.rotateCamera();

		}

		if ( !_this.noZoom ) {

			_this.zoomCamera();
			if (_this.sliderZoomFactor >= 0){
				_eye.setLength(_this.sliderZoomFactor);
				_this.sliderZoomFactor = -1;
			}

		}

		if ( !_this.noPan ) {

			_this.panCamera();

		}

		_this.object.position.add( _this.target, _eye );

		_this.checkDistances();

		_this.object.lookAt( _this.target );

		if ( lastPosition.distanceToSquared( _this.object.position ) > 0 ) {

			_this.dispatchEvent( changeEvent );

			lastPosition.copy( _this.object.position );

		}

	};

	// listeners

	function keydown( event ) {

		if ( ! _this.enabled ) return;

		window.removeEventListener( 'keydown', keydown );

		_prevState = _state;

		if ( _state !== STATE.NONE ) {

			return;

		} else if ( event.keyCode === _this.keys[ STATE.ROTATE ] && !_this.noRotate ) {

			_state = STATE.ROTATE;

		} else if ( event.keyCode === _this.keys[ STATE.ZOOM ] && !_this.noZoom ) {

			_state = STATE.ZOOM;

		} else if ( event.keyCode === _this.keys[ STATE.PAN ] && !_this.noPan ) {

			_state = STATE.PAN;

		}

	}

	function keyup( event ) {

		if ( ! _this.enabled ) return;

		_state = _prevState;

		window.addEventListener( 'keydown', keydown, false );

	}

	function mousedown( event ) {
		if ( ! _this.enabled ) return;

		event.preventDefault();
		event.stopPropagation();

		if ( _state === STATE.NONE ) {

			_state = event.button;

		}

		if ( _state === STATE.ROTATE && !_this.noRotate ) {

			_rotateStart = _rotateEnd = _this.getMouseProjectionOnBall( event.clientX, event.clientY );
            
		} else if ( _state === STATE.ZOOM && !_this.noZoom ) {

			_zoomStart = _zoomEnd = _this.getMouseOnScreen( event.clientX, event.clientY );

		} else if ( _state === STATE.PAN && !_this.noPan ) {

			_panStart = _panEnd = _this.getMouseOnScreen( event.clientX, event.clientY );

		}

		document.addEventListener( 'mousemove', mousemove, false );
		document.addEventListener( 'mouseup', mouseup, false );

	}

	function mousemove( event ) {

		if ( ! _this.enabled ) return;

		if ( _state === STATE.ROTATE && !_this.noRotate ) {

			_rotateEnd = _this.getMouseProjectionOnBall( event.clientX, event.clientY );
			_this._hasrotated = true; 
			
		} else if ( _state === STATE.ZOOM && !_this.noZoom ) {

			_zoomEnd = _this.getMouseOnScreen( event.clientX, event.clientY );

		} else if ( _state === STATE.PAN && !_this.noPan ) {

			_panEnd = _this.getMouseOnScreen( event.clientX, event.clientY );

		}

	}

	function mouseup( event ) {

		if ( ! _this.enabled ) return;

		event.preventDefault();
		event.stopPropagation();

		_state = STATE.NONE;

		document.removeEventListener( 'mousemove', mousemove );
		document.removeEventListener( 'mouseup', mouseup );

	}

	function mousewheel( event ) {

		if ( ! _this.enabled ) return;

		event.preventDefault();
		event.stopPropagation();

		var delta = 0;

		if ( event.wheelDelta ) { // WebKit / Opera / Explorer 9

			delta = event.wheelDelta / 40;

		} else if ( event.detail ) { // Firefox

			delta = - event.detail / 3;

		}

		_zoomStart.y += ( 1 / delta ) * 0.05;

	}
	

	function touchstart( event ) {

		if ( ! _this.enabled ) return;

		event.preventDefault();

		switch ( event.touches.length ) {

			case 1:
				_rotateStart = _rotateEnd = _this.getMouseProjectionOnBall( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;
			case 2:
				_zoomStart = _zoomEnd = _this.getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;
			case 3:
				_panStart = _panEnd = _this.getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;

		}

	}

	function touchmove( event ) {

		if ( ! _this.enabled ) return;

		event.preventDefault();

		switch ( event.touches.length ) {

			case 1:
				_rotateEnd = _this.getMouseProjectionOnBall( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;
			case 2:
				_zoomEnd = _this.getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;
			case 3:
				_panEnd = _this.getMouseOnScreen( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;

		}

	}

	this.domElement.addEventListener( 'contextmenu', function ( event ) { event.preventDefault(); }, false );

	this.domElement.addEventListener( 'mousedown', mousedown, false );

	this.domElement.addEventListener( 'mousewheel', mousewheel, false );
	this.domElement.addEventListener( 'DOMMouseScroll', mousewheel, false ); // firefox

	this.domElement.addEventListener( 'touchstart', touchstart, false );
	this.domElement.addEventListener( 'touchend', touchstart, false );
	this.domElement.addEventListener( 'touchmove', touchmove, false );

	window.addEventListener( 'keydown', keydown, false );
	window.addEventListener( 'keyup', keyup, false );
	this.handleResize();
	
	
	//SLIDER
	this.zoomToSlider = function(d){
		return Math.max(100- Math.log(d+1)*22, 0);
	}
	
	
	this.sliderToZoom = function(s){
		return Math.max(Math.exp((100-s)/22)-1 , 0.005);
	}
	
	
	this.sliderZoom = function(d){
		_this.sliderZoomFactor = d;
	}

};
