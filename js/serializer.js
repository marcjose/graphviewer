/**
 * @author Markus
 * 
 * Serialize objects so they can be passed by url or cookie. 
 * Method: Real object => data object => url => data object => real object 
 */


var Serializer = {};


/**
 * Create a data object from a given list of keys
 * @param {Object} obj the object to progress
 * @param {Array} keys an array of keys
 */
Serializer.makeDataObject = function(obj, keys){
    var result = {};
    for (var i=0; i<keys.length; i++){
        result[keys[i]] = obj[keys[i]];
    }
    return result;
}


/**
 * Serialize a data object to an url
 * @param {Object} data the data to serialize
 * @param {Array} keys the keys from data to serialize
 */
Serializer.serializeData = function(data, keys){
    var url = "";
    for (var i=0; i<keys.length; i++){
        if (data[ keys[i] ] !== undefined){
            if (i!=0) url += "&";
            url += encodeURIComponent(keys[i]) + "=" + encodeURIComponent(data[ keys[i] ]);
        }
    }
    return url;
}


/**
 * Creates a data object from an url
 * @param {Object} url
 */
Serializer.unserializeData = function(url, keysNumber){
    var data = url.split("&").reduce(function(prev, curr, i, arr) {
        var p = curr.split("=");
        prev[decodeURIComponent(p[0])] = decodeURIComponent(p[1]);
        return prev;
    }, {});
    if (keysNumber !== undefined){
        for (var i=0; i<keysNumber.length; i++){
            data[keysNumber[i]] = Number(data[keysNumber[i]]);
        }
    }
    return data;
}


/**
 * Sets all data values to an object
 * @param {Object} data the data values
 * @param {Object} obj  the object to set the values
 * @param {Array} keys  the keys which should be set
 */
Serializer.setDataToObject = function(data, obj, keys){
    for (var i=0; i<keys.length; i++){
        obj[ keys[i] ] = data[ keys[i] ];
    }
}



// === GRAPH ===

Serializer.keysGraph = ["term", "drawFromX","drawFromY","drawToX","drawToY", "detailsX", "detailsY", "cfn", "visible", "visibleWireframe", "transparent"];
Serializer.keysGraphNumber = ["drawFromX","drawFromY","drawToX","drawToY", "detailsX", "detailsY"];

Serializer.serializeGraph = function(graph){
    // colorfunction table lookup
    for (graph.cfn = 0; graph.cfn < ColorFunctions.list.length; graph.cfn++){
        if (graph.colorFunction == ColorFunctions.list[graph.cfn]) break;
    }
    
    return Serializer.serializeData( Serializer.makeDataObject(graph, Serializer.keysGraph) , Serializer.keysGraph);
}   

Serializer.unserializeGraph = function(url, graph){
    Serializer.setDataToObject(Serializer.unserializeData(url, Serializer.keysGraphNumber) , graph , Serializer.keysGraph);
    //update
    graph.setVisible(graph.visible == "true", graph.visibleWireframe == "true");
    graph.setTransparent(graph.transparent == "true");
    graph.colorFunction = ColorFunctions.list[graph.cfn];
    graph.setTerm(graph.term !== undefined ? graph.term : "");   
}

Serializer.unserializeToGraph = function(url){
    var g = new Graph();
    Serializer.unserializeGraph(url, g);
    return g;
}


Serializer.keysVector3 = ["x", "y", "z"];
Serializer.serializeVector3 = function(vec){
    return Serializer.serializeData( Serializer.makeDataObject(vec, Serializer.keysVector3) , Serializer.keysVector3);
}
Serializer.unserializeVector3 = function(url, vector){
    Serializer.setDataToObject(Serializer.unserializeData(url, Serializer.keysVector3) , vector, Serializer.keysVector3);
}



Serializer.loadGraph = function(url){
    var g = Serializer.unserializeToGraph(url);
    gui.addFormulaGraph(g);
    g.setVisible(g.visible, g.visibleWireframe); //refresh buttons
    g.setTransparent(g.transparent);
}

Serializer.serializeScene = function(graphset){
    var data = {};
    //Graphs
    for (var i=0; i<graphset.graphs.length; i++){
        if (i==0){ data.graphs = ""; }else{ data.graphs += "#"; }
        data.graphs += encodeURIComponent(Serializer.serializeGraph(graphset.graphs[i]));
    }
    
    //Camera
    data.cameraPos    = encodeURIComponent(Serializer.serializeVector3(engine.camera.position));
    data.cameraTarget = encodeURIComponent(Serializer.serializeVector3(engine.controls1.target));
    
    //KOS
    data.showKOS = engine.axes.visible;
    data.showGrid = engine.coordinateGrid.visible;
    
    //Intersection
    if (gui.intersect1 !== null && gui.intersect2 !== null){
        for (var i=0; i<graphset.graphs.length; i++){
            if (graphset.graphs[i] == gui.intersect1) data.intersect1 = i;
            if (graphset.graphs[i] == gui.intersect2) data.intersect2 = i;
        }
    }
    
    return Serializer.serializeData(data, ["graphs", "cameraPos", "cameraTarget", "showKOS", "showGrid", "intersect1", "intersect2"]);
}

Serializer.unserializeScene = function(url){
    //clear old scene
    var i = graphset.graphs.length;
    while(i--){
        gui.removeFormula(graphset.graphs[i]);
    }
    
    var data = Serializer.unserializeData(url);
    //Graphs
    var graphs = decodeURIComponent(data.graphs).split("#");
    for (var i = 0; i < graphs.length; i++){
        Serializer.loadGraph(graphs[i]);
    }
    
    //Camera
    Serializer.unserializeVector3(decodeURIComponent(data.cameraPos), engine.camera.position);
    Serializer.unserializeVector3(decodeURIComponent(data.cameraTarget), engine.controls1.target);
    
    //KOS
    engine.axes.setVisible(data.showKOS != "true");
    engine.coordinateGrid.setVisible(data.showGrid != "true");
    gui.showAxes();
    gui.showGrid();
    
    //Intersection
    if (data.intersect1 !== undefined){
        gui.intersect1 = graphset.graphs[Number(data.intersect1)];
        gui.intersect2 = graphset.graphs[Number(data.intersect2)];
        gui.updateIntersection();
    }
    
    gui.refreshParams();
    
    for (var i = 0; i<graphset.graphs.length; i++){
        graphset.graphs[i].gui.input.value = graphset.graphs[i].term;
        graphset.graphs[i].term = null;
        gui.functionTermChange(graphset.graphs[i]);
    }
}








// === Real URL handling ===
/**
 * Returns true if something has been loaded from the url
 */
Serializer.loadURL = function(){
    var params = document.URL.split("?");
    if (params.length < 2) return false;
    Serializer.unserializeScene(params[1]);
    return true;
}

Serializer.getURL = function(){
    var params = document.URL.split("?");
    return params[0]+"?"+Serializer.serializeScene(graphset);
}




/* TESTING
var keys = ["a", "b", "c", "d"];
var a = {a: 1, b: 0.01, c: "abc", d: "a&b&c=d", e: function(){return 0;}};
console.log(a);
var b = Serializer.makeDataObject(a, keys);
console.log(b);
var c = Serializer.serializeData(b, keys);
console.log(c);
var d = Serializer.unserializeData(c);
console.log(d);
var e = {e: "a"};
Serializer.setDataToObject(d, e, keys);
console.log(e);
*/ 
