/**
 * @author Markus
 */

//start animation loop
function animate() {
    requestAnimationFrame( animate );
    engine.render();
}

function is_touch_device() {
	return !!('ontouchstart' in window) // works on most browsers 
    	|| !!('onmsgesturechange' in window); // works on ie10
}

//3d area
function Engine(container){
    var engine = this;
    this.container = container;
    this.containerResize = function(){
        engine.renderer.setSize(engine.container.clientWidth, engine.container.clientHeight);
        engine.camera.aspect = engine.container.clientWidth/engine.container.clientHeight;
        engine.camera.updateProjectionMatrix();
        engine.controls1.handleResize();
    }
    
    if (webGLSupported()){
        this.renderer = new THREE.WebGLRenderer({
                    antialias             : !is_touch_device(), 
                    preserveDrawingBuffer : true  
                });
    }else{
        this.renderer = new THREE.CanvasRenderer();
    }
    this.renderer.setClearColorHex(0xeeffff, 1);
       
    this.VIEW_ANGLE = 45,
    this.NEAR = 0.05,
    this.FAR = 1000;
    this.camera = new THREE.PerspectiveCamera(this.VIEW_ANGLE, this.container.clientWidth/this.container.clientHeight, this.NEAR, this.FAR);

    this.scene = new THREE.Scene();

    // add the camera to the scene
    this.scene.add(this.camera);

    // start the renderer
    this.renderer.setSize(this.container.clientWidth, this.container.clientHeight);

    // attach the render-supplied DOM element
    this.container.appendChild(this.renderer.domElement);
    


    //=== LIGHTS ===
    //make a directional light
    this.lightDirectional = new THREE.DirectionalLight(0xffffff, 0.6);
    this.lightDirectional.position.set(1,-1,1); //top-right-front :D
    this.scene.add(this.lightDirectional);
    //ambient light
    this.lightAmbient = new THREE.AmbientLight(0x303030);
    this.scene.add(this.lightAmbient);
    
    this.lightPoint = new THREE.PointLight(0xffffff, 0.8, 100);
    this.lightPoint.position.set(-10, -10, 0);
    this.scene.add(this.lightPoint);
    
    //=== CONTROLS ===
    this.controls1 = new THREE.TrackballControls( this.camera , this.container );
    this.controls1.target.set( 0, 0, 0 );
    this.controls1.rotateSpeed = 2.1;
    this.controls1.rotateXSpeedup = 1.2;
    this.controls1.maxXYInclination = 40;
    //this.controls1.staticMoving = true;
    this.controls1.keys = [ 65, 83, 68 ];
    
    this.controls2 = new MouseControl(this.camera, this.container);
       
    this.container.onresize = this.containerResize;
    window.onresize = this.containerResize;
       
    //renders the current scene
    this.render = function(){
    	//stats.begin();
    	
        engine.renderer.render(this.scene, this.camera);
        engine.controls1.update();
        engine.axes.adjustMarkerTurnings();
        
        //stats.end();
    }
    
    //goes to start position
    this.defaultPosition = function (){
        engine.camera.position.x =  3;
        engine.camera.position.y = -4;
        engine.camera.position.z =  4;
        engine.camera.lookAt.x = 0;
        engine.camera.lookAt.y = 0;
        engine.camera.lookAt.z = 0;
        engine.camera.up.set(0,0,1);
        engine.controls1.target.set(0,0,0);
        engine.controls1.callOnZoom = true;
    }
     
    //CREATE COORDINATE SYSTEMS
    this.axes = new CoordinateAxes(this);
    this.coordinateGrid = new CoordinateGrid(new THREE.Vector3(-2,-2,-3), new THREE.Vector3(2,2,3), new THREE.Vector3(-50,-50,-50), new THREE.Vector3(50,50,50), 1, {linewidth: 2, color: 0xaaaaaa, transparent: true, opacity: 0.8}, this);
   	  
    this.defaultPosition();
}

/**
 * Build a KOS, directional lines. 
 * @param {Object} from Vector to start the lines from
 * @param {Object} to   Vector to end the lines
 * @param {Object} linesFrom    Vector where the lines start from, determines the length
 * @param {Object} linesTo      Vector where the lines end, determines the line length
 * @param {number} step         Which distance should the lines have
 * @param {Object} style        Settings for the line material, eg. {color: 0x000000}
 */
function CoordinateGrid(from,to, linesFrom,linesTo, step, style, _engine){
    if (_engine == undefined) _engine = engine;
	var _this = this;
	this.geoXY = this.geoXZ = this.geoYZ = null;
	this.linesXY = this.linesXZ = this.linesYZ = null;
	this.style = style;
	this.visible = false;
	this.step = step;
	this.from = from;
	this.to   = to;
	this.linesFrom = linesFrom;
	this.linesTo   = linesTo;
	
	this.createLines = function(from,to,linesFrom,linesTo,step){
		/*
		//YZ-Grid
		_this.geoYZ = new THREE.Geometry();
		for (var y = -50; y <= 50; y = y+step){
			_this.geoYZ.vertices.push(new THREE.Vector3(0,y,linesFrom.z));
			_this.geoYZ.vertices.push(new THREE.Vector3(0,y,linesTo.z));
				
			_this.geoYZ.vertices.push(new THREE.Vector3(y,0,linesFrom.z));
			_this.geoYZ.vertices.push(new THREE.Vector3(y,0,linesTo.z));
		}
		
		//XY-Grid
		_this.geoXY = new THREE.Geometry();
		for (var y = -50; y <= 50; y = y+step){
			_this.geoXY.vertices.push(new THREE.Vector3(linesFrom.x,y,0));
			_this.geoXY.vertices.push(new THREE.Vector3(linesTo.x,y,0));
				
			_this.geoXY.vertices.push(new THREE.Vector3(y,linesFrom.y,0));
			_this.geoXY.vertices.push(new THREE.Vector3(y,linesTo.y,0));
		}
		
		//XZ-Grid
		_this.geoXZ = new THREE.Geometry();
		for (var y = -50; y <= 50; y = y+step){
			_this.geoXZ.vertices.push(new THREE.Vector3(linesFrom.x,0,y));
			_this.geoXZ.vertices.push(new THREE.Vector3(linesTo.x,0,y));
				
			_this.geoXZ.vertices.push(new THREE.Vector3(y,0,linesFrom.z));
			_this.geoXZ.vertices.push(new THREE.Vector3(y,0,linesTo.z));
		}
			
		//remove old lines
		if (_this.linesXY != null){ engine.scene.remove(_this.linesXY); }
		if (_this.linesXZ != null){ engine.scene.remove(_this.linesXZ); }
		if (_this.linesYZ != null){ engine.scene.remove(_this.linesYZ); }
		
		_this.linesXY = new THREE.Line(_this.geoYZ, new THREE.LineBasicMaterial(_this.style), THREE.LinePieces);
		_this.linesXZ = new THREE.Line(_this.geoXZ, new THREE.LineBasicMaterial(_this.style), THREE.LinePieces);
		_this.linesYZ = new THREE.Line(_this.geoXY, new THREE.LineBasicMaterial(_this.style), THREE.LinePieces);
		*/			
		_this.geoXY = new THREE.Geometry();
		for (var x = from.x; x <= to.x; x += step){
			for (var y = from.y; y <= to.y; y = y+step){
				_this.geoXY.vertices.push(new THREE.Vector3(x,y,linesFrom.z));
				_this.geoXY.vertices.push(new THREE.Vector3(x,y,linesTo.z));
				}
		}
		
		//x-z
		_this.geoXZ = new THREE.Geometry();
		for (var x = from.x; x <= to.x; x += step){	
			for (var z = from.z; z <= to.z; z += step){
				_this.geoXZ.vertices.push(new THREE.Vector3(x,linesFrom.y,z));
				_this.geoXZ.vertices.push(new THREE.Vector3(x,linesTo.y,z));
			}
		}
		
		//y-z
		_this.geoYZ = new THREE.Geometry();
		for (var z = from.z; z <= to.z; z += step){
			for (var y = from.y; y <= to.y; y = y+step){
				_this.geoYZ.vertices.push(new THREE.Vector3(linesFrom.x,y,z));
				_this.geoYZ.vertices.push(new THREE.Vector3(linesTo.x,y,z));
			}
		}
		
		//remove old lines
		if (_this.linesXY != null){ engine.scene.remove(_this.linesXY); }
		if (_this.linesXZ != null){ engine.scene.remove(_this.linesXZ); }
		if (_this.linesYZ != null){ engine.scene.remove(_this.linesYZ); }
		
		_this.linesXY = new THREE.Line(_this.geoXY, new THREE.LineBasicMaterial(_this.style), THREE.LinePieces);
		_this.linesXZ = new THREE.Line(_this.geoXZ, new THREE.LineBasicMaterial(_this.style), THREE.LinePieces);
		_this.linesYZ = new THREE.Line(_this.geoYZ, new THREE.LineBasicMaterial(_this.style), THREE.LinePieces);
	}
	
	
	this.setVisible = function(visible){
	    if (_this.visible == visible) return;
	    _this.visible = visible;
	    var f = function(x){
            if (x == null) return;
            if (visible){_engine.scene.add(x); }else{_engine.scene.remove(x); }
        }
	    f(_this.linesXY);
        f(_this.linesXZ);
        f(_this.linesYZ);
	}
	
	this.update = function(from,to,linesFrom,linesTo){
		if (vector3Equal(from, _this.from) && vector3Equal(to, _this.to) && 
		    vector3Equal(linesFrom, _this.linesFrom) && vector3Equal(linesTo, _this.linesTo)) return;
		var v = _this.visible;
		_this.setVisible(false);
		_this.createLines(from,to,linesFrom,linesTo,_this.step);
		_this.setVisible(v);
	}
	
	this.createLines(from, to, linesFrom, linesTo, step, style);
	this.setVisible(true);
}

/**
 * 
 * @param {Object} to       A Vector that you want to show 
 * @param {Object} width    His drawing width, according to the coordinate system
 * @param {Object} material His material
 * @param {Object} arrow    Draw an arrow. Default is true.
 */
function VectorMesh(to, width, material, arrow){
    //default
    if (arrow == undefined) arrow = true;
    
    var _vectormesh = this;
    this.to = to.clone();
    this.width = width;
    
    var arrowwidth = arrow ? this.width*4 : 0;
    this.geo = new THREE.CylinderGeometry(this.width/2, this.width/2, this.to.length()-arrowwidth, 16, 4, false);
    this.geo.applyMatrix(new THREE.Matrix4().translate( new THREE.Vector3(0, (this.to.length()-arrowwidth)/2, 0)));
    if (arrow){
        geo2 = new THREE.CylinderGeometry(0, arrowwidth/2, arrowwidth, 32, 2, false);
        geo2.applyMatrix(new THREE.Matrix4().translate( new THREE.Vector3(0, this.to.length()-arrowwidth/2, 0)));
        THREE.GeometryUtils.merge(this.geo, geo2);
    }
    
    //now build a rotation matrix to rotate (0,1,0) to target. Equations have been solved on paper.
    var matrix = new THREE.Matrix3();
    var n  = this.to.clone();
        n.normalize();
    var cz = Math.sqrt(1-Math.pow(n.x,2));
    if (cz == 0){ 
        matrix.elements[0] = 0;    matrix.elements[3] = n.x; matrix.elements[6] = 0;
        matrix.elements[1] = -n.x; matrix.elements[4] = 0;   matrix.elements[7] = 0;
        matrix.elements[2] = 0;    matrix.elements[5] = 0;   matrix.elements[8] = 1;
    }else{
        var qsx = -n.z/cz;
        var qcx = n.y/cz;
        matrix.elements[0] = cz;       matrix.elements[3] = n.x;   matrix.elements[6] = 0;
        matrix.elements[1] = -qcx*n.x; matrix.elements[4] = n.y;   matrix.elements[7] = qsx;
        matrix.elements[2] = qsx*n.x;  matrix.elements[5] = n.z;   matrix.elements[8] = qcx;
    }
    this.geo.applyMatrix(matrix);
    //make mesh
    this.mesh = new THREE.Mesh(this.geo, material);
    
    /**
     * Shows the vector - adds to scene
     */
    this.show = function(){
        engine.scene.add(_vectormesh.mesh);
    }
    
    /**
     * Hides the vector - removes from scene
     */
    this.hide = function(){
        engine.scene.remove(_vectormesh.mesh);
    }
    
}

/**
 * Create a new coorinate system
 */
function CoordinateAxes(engine){
    var _coordinateAxes = this;
    this.vecX = this.vecY = this.vecZ = null;
    this.markersX = this.markersY = this.markersZ = null;
    this.defaultWidth = 0.02;
    this.material = new THREE.MeshBasicMaterial({color: 0x000000, shininess: 40});
    this.materialMarkers = new THREE.MeshBasicMaterial({color: 0x000000, shininess: 40});
    this.markerTurning = [true,true,true]; //markers[x,y,z] have default turning?
    this.visible = false;
    
    //create the default arrows with or without arrows 
    this.makeVectors = function (arrows,vSize){
        var v = _coordinateAxes.visible;
        if (v){ _coordinateAxes.setVisible(false); }
        _coordinateAxes.vecX = new VectorMesh(new THREE.Vector3(vSize.x,0,0), _coordinateAxes.defaultWidth, _coordinateAxes.material, arrows);
        _coordinateAxes.vecY = new VectorMesh(new THREE.Vector3(0,vSize.y,0), _coordinateAxes.defaultWidth, _coordinateAxes.material, arrows);
        _coordinateAxes.vecZ = new VectorMesh(new THREE.Vector3(0,0,vSize.z), _coordinateAxes.defaultWidth, _coordinateAxes.material, arrows);
        if (v){ _coordinateAxes.setVisible(true); }
    }
   
    this.adjustMarkerTurnings = function(){
        if (!_coordinateAxes.visible || _coordinateAxes.markersX == null || _coordinateAxes.markersY == null) return;
        if ((Math.abs(engine.camera.position.y) < Math.abs(engine.camera.position.z)) != _coordinateAxes.markerTurning[0]){
            _coordinateAxes.markerTurning[0] = !_coordinateAxes.markerTurning[0];
            _coordinateAxes.markersX.rotation.x = _coordinateAxes.markerTurning[0] ? 0: Math.PI/2;
        }
        
        if ((Math.abs(engine.camera.position.x) < Math.abs(engine.camera.position.z)) != _coordinateAxes.markerTurning[1]){
            _coordinateAxes.markerTurning[1] = !_coordinateAxes.markerTurning[1];
            _coordinateAxes.markersY.rotation.y = _coordinateAxes.markerTurning[1] ? 0: Math.PI/2;
        }
        
        if ((Math.abs(engine.camera.position.y) < Math.abs(engine.camera.position.x)) != _coordinateAxes.markerTurning[2]){
            _coordinateAxes.markerTurning[2] = !_coordinateAxes.markerTurning[2];
            _coordinateAxes.markersZ.rotation.z = _coordinateAxes.markerTurning[2] ? 0: Math.PI/2;
        }
    }    
    
    //make small and bigger markers
    this.makeMarkers = function(from, to, small, big, smallSize, bigSize){
        //helper, creates one x-axis where from,to are numbers
        var f = function(from,to){
            var fromSmall = Math.ceil(from/small);
            var fromBig   = Math.ceil(from/big);
            var geo = new THREE.Geometry();
            var geo2;
            //small
            for (i = Math.max(fromSmall,1); i*small <= to; i++){
                if (i*small % big != 0){
                    geo2 = new THREE.CubeGeometry(smallSize/2, 2*smallSize, smallSize, 1,1,1);
                    geo2.applyMatrix(new THREE.Matrix4().translate(new THREE.Vector3(i*small, 0,0)));
                    THREE.GeometryUtils.merge(geo,geo2);
                }
            }
            for (i = Math.min(fromSmall,0); i*small < 0; i++){
                if (i*small % big != 0){
                    geo2 = new THREE.CubeGeometry(smallSize/2, 2*smallSize, smallSize, 1,1,1);
                    geo2.applyMatrix(new THREE.Matrix4().translate(new THREE.Vector3(i*small, 0,0)));
                    THREE.GeometryUtils.merge(geo,geo2);
                }
            }
             //big
            for (i = Math.max(fromBig,1); i*big <= to; i++){
                geo2 = new THREE.CubeGeometry(bigSize/2, 2*bigSize, smallSize, 1,1,1);  //smallsize: every dash as deep as the axis
                geo2.applyMatrix(new THREE.Matrix4().translate(new THREE.Vector3(i*big, 0,0)));
                THREE.GeometryUtils.merge(geo,geo2);
            }
            for (i = Math.min(fromBig,0); i*big < 0; i++){
                geo2 = new THREE.CubeGeometry(bigSize/2, 2*bigSize, smallSize, 1,1,1);
                geo2.applyMatrix(new THREE.Matrix4().translate(new THREE.Vector3(i*big, 0,0)));
                THREE.GeometryUtils.merge(geo,geo2);
            }
            return geo;
        }
        
        //start
        var v = _coordinateAxes.visible;
        if (v){ _coordinateAxes.setVisible(false); }
        
        _coordinateAxes.markersX = new THREE.Mesh(f(from.x, to.x), _coordinateAxes.materialMarkers);
        var geo = f(from.y, to.y);
        geo.applyMatrix(rotationMatrixXY());
        _coordinateAxes.markersY = new THREE.Mesh(geo, _coordinateAxes.materialMarkers);
        geo = f(from.z, to.z);
        geo.applyMatrix(rotationMatrixXZ());
        _coordinateAxes.markersZ = new THREE.Mesh(geo, _coordinateAxes.materialMarkers);  
        if (v){ _coordinateAxes.setVisible(true); }
        _coordinateAxes.markerTurning[0] = _coordinateAxes.markerTurning[1] = _coordinateAxes.markerTurning[2] = true;
        _coordinateAxes.adjustMarkerTurnings();
    }    
    
    //adds or removes from scene
    this.setVisible = function (visible){
        if (_coordinateAxes.visible == visible) return;
        _coordinateAxes.visible = visible;
        //helper to add/remove
        var f = function(x){
            if (x == null) return;
            if (visible){engine.scene.add(x); }else{engine.scene.remove(x); }
        }
        
        f(_coordinateAxes.vecX.mesh);
        f(_coordinateAxes.vecY.mesh);
        f(_coordinateAxes.vecZ.mesh);
        f(_coordinateAxes.markersX);
        f(_coordinateAxes.markersY);
        f(_coordinateAxes.markersZ);
        
        if(visible){
            _coordinateAxes.adjustMarkerTurnings();
        }
    }
    
    this.from = null;
    this.to   = null;
    this.update = function(from, to){
    	if (_coordinateAxes.from !== null && _coordinateAxes.to !== null){
    		if (vector3Equal(_coordinateAxes.from, from) && vector3Equal(_coordinateAxes.to, to)) return;
    	}
    	var v = _coordinateAxes.visible;
    	_coordinateAxes.setVisible(false);
    	var diff = new THREE.Vector3();
    	diff.copy(to);
    	diff.subSelf(from);
    	diff.addSelf(new THREE.Vector3(0.4, 0.4, 0.4));
    	_coordinateAxes.makeVectors(true, diff);
    	_coordinateAxes.vecX.mesh.position.x = from.x-0.2;
    	_coordinateAxes.vecY.mesh.position.y = from.y-0.2;
    	_coordinateAxes.vecZ.mesh.position.z = from.z-0.2;
    	_coordinateAxes.makeMarkers(from, to, 0.1, 1, _coordinateAxes.defaultWidth, _coordinateAxes.defaultWidth*2)
    	_coordinateAxes.setVisible(v);
    	_coordinateAxes.from = from;
    	_coordinateAxes.to   = to;
    }
    
    //construct
    this.update(new THREE.Vector3(-1,-1,-1) , new THREE.Vector3(1,1,1));
    this.setVisible(true);
}    

function testFunc(){
    var g = new Graph(document.getElementById("formula1").value);
    graphset.add(g); //set drawing area
    //g.setColorFunction(ColorFunctions.simpleColor(new THREE.Color(0x008000)));
    /*g.drawFromX = -2;
    g.drawToX = 2;
    g.drawFromY = -2;
    g.drawToY = 2;
    g.getFunction = function(){
        return function(x,y){
            x = Math.round(x+2);
            y = Math.round(y+2);
            var a = new Array(
                [-1,-1,-1,-1,-1],
                [-1, 1,-1, 1,-1],
                [-1,-1, 1,-1,-1],
                [-1, 1,-1, 1,-1],
                [-1,-1,-1,-1,-1]
            );
            return a[y][x];
        }
    }*/
    g.renderGraph(); //generates mesh    
}
