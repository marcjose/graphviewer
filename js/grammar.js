/*******************************************************************************
 *                                                                             *
 *  Developer's note: to generate the pareser.js file from this grammar file   *
 *  after you've modified it, you will need peg.js from http://pegjs.majda.cz. *
 *  If you have installed it via node, you can simply run pegjs parser.js      *
 *  grammar.js from this directory.                                            *
 *  Note that in order to use the generated file in GraphView, you have to     *
 *  replace module.exports in line 1 of the generated parser.js file to var    *
 *  Parser                                                                   *
 *                                                                             *
 ******************************************************************************/

/* initializer declares helper functions, parsing tree objects and variables for parsing and
 * evaluating. Search for INIT_END for the begin of the actual grammar
 * definitions
 */
//INIT_START
{

	// third party library
	//set.js ==> https://github.com/jau/SetJS/blob/master/src/Set.js
	/*
	 * Copyright (c) 2011 Marcelo Criscuolo (criscuolo.marcelo [at] gmail [dot] com)
	 * Released under the MIT License.
	 */

	function Set(elements) {
		this.bag_ = [];
		var i;

		if (arguments.length > 0) { // optional args
			for (i=0; i < elements.length; i++) {
				this.add(elements[i]);
			}
		}
	}

	Set.prototype.search = function(e, start) {
		//TODO document this
		var j = this.bag_.length;
		var pivot;
		var i = arguments.length == 2 ? start : 0;

		while (i < j) {
			pivot = i + Math.floor((j - i) / 2);
			if (this.bag_[pivot] == e) {
				return pivot;
			}

			if (e > this.bag_[pivot]) {
				i = pivot + 1;
			} else {
				j = pivot;
			}
		}

		return i;
	}

	Set.prototype.add = function(e) {
		var p = this.search(e);
		if (this.bag_[p] != e) {
			this.bag_.splice(p, 0, e); // insert e at position p
		}
	}

	Set.prototype.contains = function(e) {
		var p = this.search(e);
		return (this.bag_[p] == e);
	}

	Set.prototype.size = function() {
		return this.bag_.length;
	}

	Set.prototype.getElements = function() {
		return this.bag_;
	}

	Set.prototype.equals = function(otherSet) {
		if (this.size() != otherSet.size()) {return false;}
		var i;
		for (i=0; i < this.bag_.length; i++) {
			if (this.bag_[i] != otherSet.bag_[i]) {
				return false;
			}
		}

		return true;
	}

	Set.prototype.difference = function(otherSet) {
		var result = new Set();

		if (this.size() == 0) {return result;}
		if (otherSet.size() == 0) {
			result.bag_ = this.bag_.slice(0);
			return result;
		}

		var i;
		var j = 0;
		for (i=0; i < this.bag_.length; i++) {
			if (this.bag_[i] > otherSet.bag_[j]) {
				j = otherSet.search(this.bag_[i], j); // finds First otherSet[j] Not Smaller than this[i]
				if (j == otherSet.bag_.length) {break;} // end of otherSet
			}

			if (this.bag_[i] < otherSet.bag_[j]) {
				result.bag_.push(this.bag_[i]);
			}
		}
		result.bag_ = result.bag_.concat(this.bag_.slice(i)); // adds the remaining elements, if there are any

		return result;
	}

	Set.prototype.intersection = function(otherSet) {
		var result = new Set();
		if ((this.size() == 0) || (otherSet.size() == 0)) {return result;}

		var i;
		var j = 0;
		for (i=0; i < this.bag_.length; i++) {
			j = otherSet.search(this.bag_[i], j); // finds First otherSet[j] Not Smaller than this[i]
			if (j == otherSet.bag_.length) {break;} // end of otherSet

			if (this.bag_[i] == otherSet.bag_[j]) {
				result.bag_.push(this.bag_[i]);
			}
		}

		return result;
	}

	Set.prototype.union = function(otherSet) {
		var result = new Set();
		if ((this.size() == 0) && (otherSet.size() == 0)) {return result;}

		var base, merged;
		if (this.size() > otherSet.size()) {
			base = this;
			merged = otherSet;
		} else {
			base = otherSet;
			merged = this;
		}

		result.bag_ = base.bag_.slice(0); // make a copy
		var i;
		for (i=0; i < merged.bag_.length; i++) {
			result.add(merged.bag_[i]); // add() doesn't allow repetition
		}

		return result;
	}
	//end set.js

	

        // Math functions
	/**
	 * "ooperators are simple function which have a name
         *  they are used in binary expressions, the name helps recognizing themperators"
	 */
	function add(a,b) {
		this.name = "add";
		return a+b;
	}

	function sub(a,b) {
		this.name = "sub";
		return a-b;
	}

	function mul(a,b) {
		this.name = "mul";
		return a*b;
	}

	function div(a,b) {
		this.name = "div";
		return a/b;
	}

	function pow(a,b) {
		this.name = "pow";
		return Math.pow(a,b);
	}

	function minus(a) {
		// unary minus
		this.name = "minus";
		return -a;
	}

	/* they are objects, which have
	 * @param name of he the function
	 * @param val the argument passed to the function
	 * @method derivate: takes a variablename and returns the derivate
	 *                   respecting that  variable
	 * @method eval: takes an enviroment (a mapping from names to values)
	 *               and returns the function evaluated in that context// "functions"
	 */
	
	function exp(val) {
		this.name = "exp";
		this.val = val;
		this.derivate = function(varname) {
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, inner, new exp(this.val));
		}
		this.eval = function(env) {
			return Math.exp(this.val.eval(env));
		}
	}

	function log(val) {
		this.name = "log";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			return new BinaryExpression(div, inner, this.val);
		}
		this.eval = function(env) {
			return Math.log(this.val.eval(env));
		}
	}

	function log2(val) {
		this.name = "log2";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new BinaryExpression(div, new Num(1), new BinaryExpression(mul, new Num(Math.LN2), this.val)); // 1/(val*ln(2))
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			return Math.log(this.val.eval(env))/Math.LN2;
		}
	}

	function log10(val) {
		this.name = "log10";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new BinaryExpression(div, new Num(1), new BinaryExpression(mul, new Num(Math.LN10), this.val)); // 1/(val*ln(10))
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			return Math.log(val.eval(env))/Math.LN10;
		}
	}

	function sqrt(val) {
		this.name = "sqrt";
		this.val = val;
		this.derivate = function(varname) {
			var inner = val.derivate(varname);
			return new BinaryExpression(div, inner, new BinaryExpression(mul, new Num(2), new Func(sqrt, this.val)));
		}
		this.eval = function(env) {
			return Math.sqrt(val.eval(env));
		}
	}

	function sin(val) {
		this.name = "sin";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var outer = new Func(cos, this.val);
			return new BinaryExpression(mul, inner, outer);
		}
		this.eval = function(env) {
			return Math.sin(this.val.eval(env));
		}
	}

	function cos(val) {
		this.name = "cos";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var outer = new UnaryExpression(minus, new Func(sin, this.val));
			return new BinaryExpression(mul, inner, outer);
		}
		this.eval = function(env) {
			return Math.cos(this.val.eval(env));
		}
	}

	function tan(val) {
		this.name = "tan";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var t2 = new BinaryExpression(mul, new Func(cos, this.val), new Func(cos, this.val));
			return new BinaryExpression(div, inner, t2);
		}
		this.eval = function(env) {
			return Math.tan(this.val.eval(env));
		}
	}

	function cot(val) {
		this.name = "cot";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var sin_ = new Func(sin, val);
			var sinquad = new BinaryExpression(mul, sin_, sin_);
			return new BinaryExpression(div, new UnaryExpression(minus, inner), sinquad);
		}
		this.eval = function(env) {
			return 1/(Math.tan(val.eval(env)));
		}
	}

	function asin(val) {
		this.name = "arcsin";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var outer = new Func(sqrt, new BinaryExpression(sub, new Num(1), new BinaryExpression(mul, this.val, this.val)));
			return new BinaryExpression(mul, inner, outer);
		}
		this.eval = function(env) {
			return Math.asin(this.val.eval(env));
		}
	}

	function acos(val) {
		this.name = "arccos";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var outer = new Func(sqrt, new BinaryExpression(sub, new Num(1), new BinaryExpression(mul, this.val, this.val)));
			return new BinaryExpression(mul, new UnaryExpression(minus, inner), outer);
		}
		this.eval = function(env) {
			return Math.acos(this.val.eval(env));
		}
	}

	function atan(val) {
		this.name = "arctan";
		this.val = val;
		this.derivate = function(varname) {
			var inner = this.val.derivate(varname);
			var t2 = new BinaryExpression(add, new Num(1), new BinaryExpression(mul, this.val, this.val));
			return new BinaryExpression(div, inner, t2);
		}
		this.eval = function(env) {
			return Math.atan(this.val.eval(env));
		}
	}

	function sinh(val) {
		this.name = "sinh";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new Func(cosh, this.val); //cosh(val)
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var term1 = Math.pow(Math.E, this.val.eval(env));
			var term2 = Math.pow(Math.E, -this.val.eval(env));
			return (term1 - term2)/2;
		}
	}

	function cosh(val) {
		this.name = "cosh";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new Func(sinh, this.val) //sinh(val)
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var val = this.val.eval(env);
			var term1 = Math.pow(Math.E, val);
			var term2 = Math.pow(Math.E, -val);
			return (term1 + term2)/2;
		}
	}

	function tanh(val) {
		this.name = "tanh";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new BinaryExpression(div, 1, new BinaryExpression(mul, new Func(cosh, this.val), new Func(cosh, this.val))) // 1/cosh^2(val)
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var s = new sinh(this.val).eval(env);
			var c = new cosh(this.val).eval(env);
			return s/c;
		}
	}

	function coth(val) {
		this.name = "coth";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new UnaryExpression(minus, new BinaryExpression(div, 1, new BinaryExpression(mul, new Func(sinh, this.val), new Func(sinh, this.val)))) // 1/sinh^2(val)
			var inner = val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var s = new sinh(this.val).eval(env);
			var c = new cosh(this.val).eval(env);
			return c/s;
		}
	}

	function acot(val) {
		this.name = "arccot";
		this.val = val;
		this.derivate = function(varname) {
			var inner = val.derivate(varname);
			return new BinaryExpression(div, new UnaryExpression(minus, inner), new BinaryExpression(add, new Num(1), new BinaryExpression(mul, val, val)));
		}
		this.eval = function(env) {
			return Math.PI/2*Math.atan(val.eval(env));
		}
	}

	function arsinh(val) {
		this.name = "arsinh";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new BinaryExpression(div, new Num(1), new Func(sqrt, new BinaryExpression(add, new Num(1), new BinaryExpression(mul, val, val))));
			var inner = this.val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var val = this.val.eval(env);
			return Math.log(val + Math.sqrt(val*val + 1));
		}
	}

	function arcosh(val) {
		this.name ="arcosh";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new BinaryExpression(div, new Num(1), new Func(sqrt, new BinaryExpression(sub, new Num(1), new BinaryExpression(mul, val, val))));
			var inner = this.val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var val = this.val.eval(env);
			return Math.log(val + Math.sqrt(val*val - 1));
		}
	}

	function artanh(val) {
		this.name = "artanh";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new BinaryExpression(div, new Num(1), new BinaryExpression(sub, new Num(1), new BinaryExpression(mul, val, val)));
			var inner = this.val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var val = this.val.eval(env);
			return 0.5 * Math.log((1 + val)/(1 - val));
		}
	}

	function arcoth(val) {
		this.name = "arcoth";
		this.val = val;
		this.derivate = function(varname) {
			var outer = new UnaryExpression(minus, new BinaryExpression(div, new Num(1), new BinaryExpression(sub, new BinaryExpression(mul, val, val), new Num(1))));
			var inner = this.val.derivate(varname);
			return new BinaryExpression(mul, outer, inner); 
		}
		this.eval = function(env) {
			var val = this.val.eval(env);
			return 0.5 * Math.log((val + 1)/(val - 1));
		}
	}


	// helper methods
	// flatten as implemented by http://tech.karbassi.com/2009/12/17/pure-javascript-flatten-array/
	function flatten(array){
		var flat = [];
		for (var i = 0, l = array.length; i < l; i++){
			var type = Object.prototype.toString.call(array[i]).split(' ').pop().split(']').shift().toLowerCase();
			if (type) { flat = flat.concat(/^(array|collection|arguments|object)$/.test(type) ? flatten(array[i]) : array[i]); }
		}
		return flat;
	}

	

	// Parser Objects
	/*
	 * Parser objects build up the parse tree. Each of them is used to
	 * described a subexpression of a term.
	 * All parser objects support the following methods
	 * eval(env): will evaluate the corresponding subexpression in the
	 *            context of env
	 * containsVariable(varname): checks if the expressions contains the
	 *                            variable denoted by varname
	 * listVariables(): list all variables (variables and contstants) in an
	 *                  expression and returns them as an array
	 * derivate(varname): computes the (partial) derivate with respect to
	 *                    varname
	 */
	function UnaryExpression(operator, expression) {
		/**
		 * @param operator a unary function with a name attribute
		 * @param expression a parser object
		 */
		if (expression === undefined) {throw "expression is undefined";};
		if (operator === undefined) {throw "operator is undefined";};
		this.expression = expression;
		this.operator = operator;
		this.eval = function(env) {
			return this.operator(this.expression.eval(env));
		}
		this.containsVariable = function(varname) {
			return this.expression.containsVariable(varname);
		}
		this.listVariables = function() {
			return this.expression.listVariables();
		}
		this.derivate = function(varname) {
			if (this.operator.name === "minus") {
				return new UnaryExpression(minus, expression.derivate(varname));
			} else {
				console.log("undefined operator: ");
				console.log(this.operator.name);
			}
		}
	}

	function BinaryExpression(operator, left, right) {
		/**
		 * @param operator a binary function with a name attribute
		 * @param left a parser object
		 * @param right a parser object
		 */
		if (left === undefined) {throw "left is undefined";};
		if (right === undefined) {throw "right is undefined";};
		if (operator === undefined) {throw "operator is undefined";};
		this.operator = operator;
		this.left = left;
		this.right = right;
		this.eval = function(env) {
			return this.operator(this.left.eval(env), this.right.eval(env));
		}
		this.containsVariable = function(varname) {
			return (this.left.containsVariable(varname) || this.right.containsVariable(varname) );
		}
		this.listVariables = function() {
			return this.left.listVariables().union(this.right.listVariables());
		}
		this.derivate = function(varname) {
			var leftd = left.derivate(varname);
			var rightd = right.derivate(varname);
			switch (operator.name) {
				case "add":
					return new BinaryExpression(add, leftd, rightd);
					break;
				case "sub":
					return new BinaryExpression(sub, leftd, rightd);
					break;
				case "mul":
					var lnew = new BinaryExpression(mul, leftd, right);
					var rnew = new BinaryExpression(mul, left, rightd);
					return new BinaryExpression(add, lnew, rnew);
					break;
				case "div":
					var lnew = new BinaryExpression(mul, leftd, right);
					var rnew = new BinaryExpression(mul, left, rightd);
					var denonimator = new BinaryExpression(mul, right, right);
					return new BinaryExpression(div, new BinaryExpression(sub, lnew, rnew), denonimator);
					break;
				case "pow":
					// handling special cases, because the
					// general case fails for negative values
					// checking if only base contains the variable
					// t^n = t'*t^(n-1)*n
					if (!right.containsVariable(varname)) {
						var tmp1 = new BinaryExpression(sub, right, new Num(1));
						var tmp2 = new BinaryExpression(mul, leftd, new BinaryExpression(pow, left, tmp1));
						return new BinaryExpression(mul, right, tmp2);
					}
					// checking if base doesn't contain the variable
					// f(x) = a^t(x) -> f'(x) = ln(a) * t(x) * t'(x)
					if (!left.containsVariable(varname)) {
						var tmp1 = new Func(log, right);
						var tmp2 = new BinaryExpression(mul, tmp1, right);
						return new BinaryExpression(mul, tmp2, rightd);
					}
					// general case:
					// d(u^v) = v*u^(v-1)*du+u^v*ln(u)*dv
					var tmp1 = new BinaryExpression(pow, left, new BinaryExpression(sub, right, new Num(1))); //u^(v-1)
					var tmp2 = new BinaryExpression(mul, right, tmp1); // v*u^(v-1)
					var tmp3 = new BinaryExpression(mul, tmp2, leftd); //v*u^(v-1)*du
					var tmp4 = new Func(log, left); //ln(u)
					var tmp5 = new BinaryExpression(pow, left, right); //u^v
					var tmp6 = new BinaryExpression(mul, tmp4, rightd); //ln(u)*dv
					var tmp7 = new BinaryExpression(mul, tmp5, tmp6); //u^v*ln(u)*dv
					return new BinaryExpression(add, tmp3, tmp7); 
				default:
					console.log(operator.name);
					throw "wtf is missing?";
			}
		}
	}

	function Variable(variable){ // variable in a mathmatical sense including constants
		/**
		 * @param variable the name of the variable
		 */
		if (variable === undefined) {throw "variable is undefined";};
		this.variable = variable;
		this.eval = function(env) {return env[this.variable];};
		this.containsVariable = function(varname) {
			return this.variable === varname;
		}
		this.listVariables = function() {
			return new Set(this.variable);
		}
		this.derivate = function(varname) {
			if (this.variable === varname) {
				return new Num(1);
			} else {
				return new Num(0);
			}
		}
	}

	function Func(func, arg) {
		/**
		 * @param func a unary function with a name and a derivate
		 *             function
		 * @param arg a parser object passed to the function
		 */
		if (func === undefined) {throw "func is undefined";};
		if (arg === undefined) {throw "arg is undefined";};
		this.func = new func(arg);
		this.arg = arg;
		this.eval = function(env) {return this.func.eval(env)};
		this.containsVariable = function(varname) {
			return this.arg.containsVariable(varname);
		}
		this.listVariables = function() {
			return this.arg.listVariables();
		}
		this.derivate = function(varname) {
			return this.func.derivate(varname);
		}
	}

	function Num(value) {
		this.value = value;
		this.eval = function(env) {return this.value;};
		this.containsVariable = function(varname) {
			return false;
		}
		this.listVariables = function() {
			return new Set([]);
		}
		this.derivate = function(varname) {
			return new Num(0);
		}
	}
	

} //INIT_END

start
   = expression:additive {return expression;}

additive
  = ws head:multiplicative ws tail:(addop ws multiplicative ws)* { 
	  var result = head;
	  for (var i = 0; i < tail.length; i++) {
		  result = new BinaryExpression(tail[i][0], result, tail[i][2]);
	  }
	  return result;
  }

addop
  = "+" {return add ;}
  / "-" {return sub ;}

multiplicative
  = ws head:exponentative ws tail:(mulop ws exponentative ws)* {
	  var result = head;
	  for (var i = 0; i < tail.length; i++) {
		  result = new BinaryExpression(tail[i][0], result, tail[i][2]);
	  }
	  return result;
  }

exponentative
  = ws head:factor ws tail:("^" ws factor ws)* {
	  var result = head;
	  for (var i = 0; i < tail.length; i++) {
		  result = new BinaryExpression(pow, result, tail[i][2]);
	  }
	  return result;
  }

mulop
  = "*" {return mul ;}
  / "/" {return div ;}

factor
  = sfunc:simplefunction  {return sfunc;}
  / head:(number / variable / constant) tail:(variable / constant)* {
	  var result = head;
	  for (var i = 0; i < tail.length; i++) {
		  console.log(tail[i][0]);
		  result = new BinaryExpression(mul, result, tail[i]);
	  }
	  return result;
  }
  / number:number {return number;}
  / "(" additive:additive ")" {return additive;} 
  / unary:unop factor:factor {return new UnaryExpression(unary, factor);}

unop
  = "-" {return minus;}

simplefunction // simple because there is neither recursion nor complex numbers
  = tri:trigeomfunction {return tri;}
  / other:otherfunction {return other;}

trigeomfunction // e.g. sin(x), atan(42+12*(22-x), sin^{2}x
  = funcname:trifuncname "(" ws arg:additive ws ")" {return new Func(funcname, arg);}
  //FIXME: allow this, but needs a new evaluable object
  /// funcname:trifuncname "^" num:integer arg:variable {return Math.pow(funcname(arg), num);}

trifuncname 
  = "sinh" {return sinh; }
  / "cosh" {return  cosh; }
  / "tanh" {return tanh; }
  / "coth" {return coth; }
  / "arsinh" {return arsinh; }
  / "arcosh" {return arcosh; }
  / "cos" {return cos; }
  / "sin" {return sin; }
  / "tan" {return tan; }
  / "cot" {return cot; }
  / "arcsin" {return asin; }
  / "arccos" {return acos; }
  / "arctan" {return atan; }
  / "arccot" {return acot; }

otherfunction
  = "pow" "(" ws base:additive ws "," ws exp:additive ws ")" {return new BinaryExpression(pow, base,exp);} //well, exp is an operator in some way
  / "ln" "(" ws arg:additive ws ")" {return new Func(log,arg); } // natural logarithm
  / "lb" "(" ws arg:additive ws ")" {return  new Func(log2, arg); }
  / "log" "(" ws arg:additive ws ")" {return new Func(log10,arg);}
  / "sqrt" "(" ws arg:additive ws ")" {return new Func(sqrt, arg);}

number // parses e.g. 12.42, 000003, 0.13, 42001
  = digits:([0-9]+ ("." [0-9]*)?) {return  new Num(parseFloat(flatten(digits).join(""), 10));}

integer
  = digits:[0-9]+ { return new Num(parseInt(digits.join(""), 10)); }

constant
  = "e" {return new Num(Math.E);}
  / "PI" {return new Num(Math.PI); }
  / constant:[a-df-w] {return new Variable(constant); }

variable
  = "x" {return new Variable("x"); }
  / "y" {return new Variable("y"); }

ws // whitespace
 = " "*
